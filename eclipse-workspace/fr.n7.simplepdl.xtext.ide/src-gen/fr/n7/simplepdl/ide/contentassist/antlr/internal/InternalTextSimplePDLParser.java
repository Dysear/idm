package fr.n7.simplepdl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import fr.n7.simplepdl.services.TextSimplePDLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalTextSimplePDLParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_INT", "RULE_ID", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'s2s'", "'s2f'", "'f2s'", "'f2f'", "'process'", "'{'", "'}'", "'WorkDefinition'", "'WorkSequence'", "'from'", "'to'", "';'", "'Guidance'", "'description'", "'Resources'", "'uses'", "'of'", "'-'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=4;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalTextSimplePDLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalTextSimplePDLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalTextSimplePDLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalTextSimplePDL.g"; }


    	private TextSimplePDLGrammarAccess grammarAccess;

    	public void setGrammarAccess(TextSimplePDLGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleProcess"
    // InternalTextSimplePDL.g:53:1: entryRuleProcess : ruleProcess EOF ;
    public final void entryRuleProcess() throws RecognitionException {
        try {
            // InternalTextSimplePDL.g:54:1: ( ruleProcess EOF )
            // InternalTextSimplePDL.g:55:1: ruleProcess EOF
            {
             before(grammarAccess.getProcessRule()); 
            pushFollow(FOLLOW_1);
            ruleProcess();

            state._fsp--;

             after(grammarAccess.getProcessRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleProcess"


    // $ANTLR start "ruleProcess"
    // InternalTextSimplePDL.g:62:1: ruleProcess : ( ( rule__Process__Group__0 ) ) ;
    public final void ruleProcess() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:66:2: ( ( ( rule__Process__Group__0 ) ) )
            // InternalTextSimplePDL.g:67:2: ( ( rule__Process__Group__0 ) )
            {
            // InternalTextSimplePDL.g:67:2: ( ( rule__Process__Group__0 ) )
            // InternalTextSimplePDL.g:68:3: ( rule__Process__Group__0 )
            {
             before(grammarAccess.getProcessAccess().getGroup()); 
            // InternalTextSimplePDL.g:69:3: ( rule__Process__Group__0 )
            // InternalTextSimplePDL.g:69:4: rule__Process__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Process__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getProcessAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleProcess"


    // $ANTLR start "entryRuleWorkDefinition"
    // InternalTextSimplePDL.g:78:1: entryRuleWorkDefinition : ruleWorkDefinition EOF ;
    public final void entryRuleWorkDefinition() throws RecognitionException {
        try {
            // InternalTextSimplePDL.g:79:1: ( ruleWorkDefinition EOF )
            // InternalTextSimplePDL.g:80:1: ruleWorkDefinition EOF
            {
             before(grammarAccess.getWorkDefinitionRule()); 
            pushFollow(FOLLOW_1);
            ruleWorkDefinition();

            state._fsp--;

             after(grammarAccess.getWorkDefinitionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWorkDefinition"


    // $ANTLR start "ruleWorkDefinition"
    // InternalTextSimplePDL.g:87:1: ruleWorkDefinition : ( ( rule__WorkDefinition__Group__0 ) ) ;
    public final void ruleWorkDefinition() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:91:2: ( ( ( rule__WorkDefinition__Group__0 ) ) )
            // InternalTextSimplePDL.g:92:2: ( ( rule__WorkDefinition__Group__0 ) )
            {
            // InternalTextSimplePDL.g:92:2: ( ( rule__WorkDefinition__Group__0 ) )
            // InternalTextSimplePDL.g:93:3: ( rule__WorkDefinition__Group__0 )
            {
             before(grammarAccess.getWorkDefinitionAccess().getGroup()); 
            // InternalTextSimplePDL.g:94:3: ( rule__WorkDefinition__Group__0 )
            // InternalTextSimplePDL.g:94:4: rule__WorkDefinition__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__WorkDefinition__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getWorkDefinitionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWorkDefinition"


    // $ANTLR start "entryRuleWorkSequence"
    // InternalTextSimplePDL.g:103:1: entryRuleWorkSequence : ruleWorkSequence EOF ;
    public final void entryRuleWorkSequence() throws RecognitionException {
        try {
            // InternalTextSimplePDL.g:104:1: ( ruleWorkSequence EOF )
            // InternalTextSimplePDL.g:105:1: ruleWorkSequence EOF
            {
             before(grammarAccess.getWorkSequenceRule()); 
            pushFollow(FOLLOW_1);
            ruleWorkSequence();

            state._fsp--;

             after(grammarAccess.getWorkSequenceRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWorkSequence"


    // $ANTLR start "ruleWorkSequence"
    // InternalTextSimplePDL.g:112:1: ruleWorkSequence : ( ( rule__WorkSequence__Group__0 ) ) ;
    public final void ruleWorkSequence() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:116:2: ( ( ( rule__WorkSequence__Group__0 ) ) )
            // InternalTextSimplePDL.g:117:2: ( ( rule__WorkSequence__Group__0 ) )
            {
            // InternalTextSimplePDL.g:117:2: ( ( rule__WorkSequence__Group__0 ) )
            // InternalTextSimplePDL.g:118:3: ( rule__WorkSequence__Group__0 )
            {
             before(grammarAccess.getWorkSequenceAccess().getGroup()); 
            // InternalTextSimplePDL.g:119:3: ( rule__WorkSequence__Group__0 )
            // InternalTextSimplePDL.g:119:4: rule__WorkSequence__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__WorkSequence__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getWorkSequenceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWorkSequence"


    // $ANTLR start "entryRuleProcessElement"
    // InternalTextSimplePDL.g:128:1: entryRuleProcessElement : ruleProcessElement EOF ;
    public final void entryRuleProcessElement() throws RecognitionException {
        try {
            // InternalTextSimplePDL.g:129:1: ( ruleProcessElement EOF )
            // InternalTextSimplePDL.g:130:1: ruleProcessElement EOF
            {
             before(grammarAccess.getProcessElementRule()); 
            pushFollow(FOLLOW_1);
            ruleProcessElement();

            state._fsp--;

             after(grammarAccess.getProcessElementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleProcessElement"


    // $ANTLR start "ruleProcessElement"
    // InternalTextSimplePDL.g:137:1: ruleProcessElement : ( ( rule__ProcessElement__Alternatives ) ) ;
    public final void ruleProcessElement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:141:2: ( ( ( rule__ProcessElement__Alternatives ) ) )
            // InternalTextSimplePDL.g:142:2: ( ( rule__ProcessElement__Alternatives ) )
            {
            // InternalTextSimplePDL.g:142:2: ( ( rule__ProcessElement__Alternatives ) )
            // InternalTextSimplePDL.g:143:3: ( rule__ProcessElement__Alternatives )
            {
             before(grammarAccess.getProcessElementAccess().getAlternatives()); 
            // InternalTextSimplePDL.g:144:3: ( rule__ProcessElement__Alternatives )
            // InternalTextSimplePDL.g:144:4: rule__ProcessElement__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__ProcessElement__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getProcessElementAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleProcessElement"


    // $ANTLR start "entryRuleGuidance"
    // InternalTextSimplePDL.g:153:1: entryRuleGuidance : ruleGuidance EOF ;
    public final void entryRuleGuidance() throws RecognitionException {
        try {
            // InternalTextSimplePDL.g:154:1: ( ruleGuidance EOF )
            // InternalTextSimplePDL.g:155:1: ruleGuidance EOF
            {
             before(grammarAccess.getGuidanceRule()); 
            pushFollow(FOLLOW_1);
            ruleGuidance();

            state._fsp--;

             after(grammarAccess.getGuidanceRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGuidance"


    // $ANTLR start "ruleGuidance"
    // InternalTextSimplePDL.g:162:1: ruleGuidance : ( ( rule__Guidance__Group__0 ) ) ;
    public final void ruleGuidance() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:166:2: ( ( ( rule__Guidance__Group__0 ) ) )
            // InternalTextSimplePDL.g:167:2: ( ( rule__Guidance__Group__0 ) )
            {
            // InternalTextSimplePDL.g:167:2: ( ( rule__Guidance__Group__0 ) )
            // InternalTextSimplePDL.g:168:3: ( rule__Guidance__Group__0 )
            {
             before(grammarAccess.getGuidanceAccess().getGroup()); 
            // InternalTextSimplePDL.g:169:3: ( rule__Guidance__Group__0 )
            // InternalTextSimplePDL.g:169:4: rule__Guidance__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Guidance__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getGuidanceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGuidance"


    // $ANTLR start "entryRuleResources"
    // InternalTextSimplePDL.g:178:1: entryRuleResources : ruleResources EOF ;
    public final void entryRuleResources() throws RecognitionException {
        try {
            // InternalTextSimplePDL.g:179:1: ( ruleResources EOF )
            // InternalTextSimplePDL.g:180:1: ruleResources EOF
            {
             before(grammarAccess.getResourcesRule()); 
            pushFollow(FOLLOW_1);
            ruleResources();

            state._fsp--;

             after(grammarAccess.getResourcesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleResources"


    // $ANTLR start "ruleResources"
    // InternalTextSimplePDL.g:187:1: ruleResources : ( ( rule__Resources__Group__0 ) ) ;
    public final void ruleResources() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:191:2: ( ( ( rule__Resources__Group__0 ) ) )
            // InternalTextSimplePDL.g:192:2: ( ( rule__Resources__Group__0 ) )
            {
            // InternalTextSimplePDL.g:192:2: ( ( rule__Resources__Group__0 ) )
            // InternalTextSimplePDL.g:193:3: ( rule__Resources__Group__0 )
            {
             before(grammarAccess.getResourcesAccess().getGroup()); 
            // InternalTextSimplePDL.g:194:3: ( rule__Resources__Group__0 )
            // InternalTextSimplePDL.g:194:4: rule__Resources__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Resources__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getResourcesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleResources"


    // $ANTLR start "entryRuleUsedResources"
    // InternalTextSimplePDL.g:203:1: entryRuleUsedResources : ruleUsedResources EOF ;
    public final void entryRuleUsedResources() throws RecognitionException {
        try {
            // InternalTextSimplePDL.g:204:1: ( ruleUsedResources EOF )
            // InternalTextSimplePDL.g:205:1: ruleUsedResources EOF
            {
             before(grammarAccess.getUsedResourcesRule()); 
            pushFollow(FOLLOW_1);
            ruleUsedResources();

            state._fsp--;

             after(grammarAccess.getUsedResourcesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUsedResources"


    // $ANTLR start "ruleUsedResources"
    // InternalTextSimplePDL.g:212:1: ruleUsedResources : ( ( rule__UsedResources__Group__0 ) ) ;
    public final void ruleUsedResources() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:216:2: ( ( ( rule__UsedResources__Group__0 ) ) )
            // InternalTextSimplePDL.g:217:2: ( ( rule__UsedResources__Group__0 ) )
            {
            // InternalTextSimplePDL.g:217:2: ( ( rule__UsedResources__Group__0 ) )
            // InternalTextSimplePDL.g:218:3: ( rule__UsedResources__Group__0 )
            {
             before(grammarAccess.getUsedResourcesAccess().getGroup()); 
            // InternalTextSimplePDL.g:219:3: ( rule__UsedResources__Group__0 )
            // InternalTextSimplePDL.g:219:4: rule__UsedResources__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__UsedResources__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getUsedResourcesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUsedResources"


    // $ANTLR start "entryRuleEInt"
    // InternalTextSimplePDL.g:228:1: entryRuleEInt : ruleEInt EOF ;
    public final void entryRuleEInt() throws RecognitionException {
        try {
            // InternalTextSimplePDL.g:229:1: ( ruleEInt EOF )
            // InternalTextSimplePDL.g:230:1: ruleEInt EOF
            {
             before(grammarAccess.getEIntRule()); 
            pushFollow(FOLLOW_1);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getEIntRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEInt"


    // $ANTLR start "ruleEInt"
    // InternalTextSimplePDL.g:237:1: ruleEInt : ( ( rule__EInt__Group__0 ) ) ;
    public final void ruleEInt() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:241:2: ( ( ( rule__EInt__Group__0 ) ) )
            // InternalTextSimplePDL.g:242:2: ( ( rule__EInt__Group__0 ) )
            {
            // InternalTextSimplePDL.g:242:2: ( ( rule__EInt__Group__0 ) )
            // InternalTextSimplePDL.g:243:3: ( rule__EInt__Group__0 )
            {
             before(grammarAccess.getEIntAccess().getGroup()); 
            // InternalTextSimplePDL.g:244:3: ( rule__EInt__Group__0 )
            // InternalTextSimplePDL.g:244:4: rule__EInt__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__EInt__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEIntAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEInt"


    // $ANTLR start "ruleWorkSequenceType"
    // InternalTextSimplePDL.g:253:1: ruleWorkSequenceType : ( ( rule__WorkSequenceType__Alternatives ) ) ;
    public final void ruleWorkSequenceType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:257:1: ( ( ( rule__WorkSequenceType__Alternatives ) ) )
            // InternalTextSimplePDL.g:258:2: ( ( rule__WorkSequenceType__Alternatives ) )
            {
            // InternalTextSimplePDL.g:258:2: ( ( rule__WorkSequenceType__Alternatives ) )
            // InternalTextSimplePDL.g:259:3: ( rule__WorkSequenceType__Alternatives )
            {
             before(grammarAccess.getWorkSequenceTypeAccess().getAlternatives()); 
            // InternalTextSimplePDL.g:260:3: ( rule__WorkSequenceType__Alternatives )
            // InternalTextSimplePDL.g:260:4: rule__WorkSequenceType__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__WorkSequenceType__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getWorkSequenceTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWorkSequenceType"


    // $ANTLR start "rule__ProcessElement__Alternatives"
    // InternalTextSimplePDL.g:268:1: rule__ProcessElement__Alternatives : ( ( ruleWorkDefinition ) | ( ruleWorkSequence ) | ( ruleGuidance ) | ( ruleResources ) );
    public final void rule__ProcessElement__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:272:1: ( ( ruleWorkDefinition ) | ( ruleWorkSequence ) | ( ruleGuidance ) | ( ruleResources ) )
            int alt1=4;
            switch ( input.LA(1) ) {
            case 18:
                {
                alt1=1;
                }
                break;
            case 19:
                {
                alt1=2;
                }
                break;
            case 23:
                {
                alt1=3;
                }
                break;
            case 25:
                {
                alt1=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalTextSimplePDL.g:273:2: ( ruleWorkDefinition )
                    {
                    // InternalTextSimplePDL.g:273:2: ( ruleWorkDefinition )
                    // InternalTextSimplePDL.g:274:3: ruleWorkDefinition
                    {
                     before(grammarAccess.getProcessElementAccess().getWorkDefinitionParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleWorkDefinition();

                    state._fsp--;

                     after(grammarAccess.getProcessElementAccess().getWorkDefinitionParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalTextSimplePDL.g:279:2: ( ruleWorkSequence )
                    {
                    // InternalTextSimplePDL.g:279:2: ( ruleWorkSequence )
                    // InternalTextSimplePDL.g:280:3: ruleWorkSequence
                    {
                     before(grammarAccess.getProcessElementAccess().getWorkSequenceParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleWorkSequence();

                    state._fsp--;

                     after(grammarAccess.getProcessElementAccess().getWorkSequenceParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalTextSimplePDL.g:285:2: ( ruleGuidance )
                    {
                    // InternalTextSimplePDL.g:285:2: ( ruleGuidance )
                    // InternalTextSimplePDL.g:286:3: ruleGuidance
                    {
                     before(grammarAccess.getProcessElementAccess().getGuidanceParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleGuidance();

                    state._fsp--;

                     after(grammarAccess.getProcessElementAccess().getGuidanceParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalTextSimplePDL.g:291:2: ( ruleResources )
                    {
                    // InternalTextSimplePDL.g:291:2: ( ruleResources )
                    // InternalTextSimplePDL.g:292:3: ruleResources
                    {
                     before(grammarAccess.getProcessElementAccess().getResourcesParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleResources();

                    state._fsp--;

                     after(grammarAccess.getProcessElementAccess().getResourcesParserRuleCall_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProcessElement__Alternatives"


    // $ANTLR start "rule__WorkSequenceType__Alternatives"
    // InternalTextSimplePDL.g:301:1: rule__WorkSequenceType__Alternatives : ( ( ( 's2s' ) ) | ( ( 's2f' ) ) | ( ( 'f2s' ) ) | ( ( 'f2f' ) ) );
    public final void rule__WorkSequenceType__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:305:1: ( ( ( 's2s' ) ) | ( ( 's2f' ) ) | ( ( 'f2s' ) ) | ( ( 'f2f' ) ) )
            int alt2=4;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt2=1;
                }
                break;
            case 12:
                {
                alt2=2;
                }
                break;
            case 13:
                {
                alt2=3;
                }
                break;
            case 14:
                {
                alt2=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalTextSimplePDL.g:306:2: ( ( 's2s' ) )
                    {
                    // InternalTextSimplePDL.g:306:2: ( ( 's2s' ) )
                    // InternalTextSimplePDL.g:307:3: ( 's2s' )
                    {
                     before(grammarAccess.getWorkSequenceTypeAccess().getStart2startEnumLiteralDeclaration_0()); 
                    // InternalTextSimplePDL.g:308:3: ( 's2s' )
                    // InternalTextSimplePDL.g:308:4: 's2s'
                    {
                    match(input,11,FOLLOW_2); 

                    }

                     after(grammarAccess.getWorkSequenceTypeAccess().getStart2startEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalTextSimplePDL.g:312:2: ( ( 's2f' ) )
                    {
                    // InternalTextSimplePDL.g:312:2: ( ( 's2f' ) )
                    // InternalTextSimplePDL.g:313:3: ( 's2f' )
                    {
                     before(grammarAccess.getWorkSequenceTypeAccess().getStart2finishEnumLiteralDeclaration_1()); 
                    // InternalTextSimplePDL.g:314:3: ( 's2f' )
                    // InternalTextSimplePDL.g:314:4: 's2f'
                    {
                    match(input,12,FOLLOW_2); 

                    }

                     after(grammarAccess.getWorkSequenceTypeAccess().getStart2finishEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalTextSimplePDL.g:318:2: ( ( 'f2s' ) )
                    {
                    // InternalTextSimplePDL.g:318:2: ( ( 'f2s' ) )
                    // InternalTextSimplePDL.g:319:3: ( 'f2s' )
                    {
                     before(grammarAccess.getWorkSequenceTypeAccess().getFinish2startEnumLiteralDeclaration_2()); 
                    // InternalTextSimplePDL.g:320:3: ( 'f2s' )
                    // InternalTextSimplePDL.g:320:4: 'f2s'
                    {
                    match(input,13,FOLLOW_2); 

                    }

                     after(grammarAccess.getWorkSequenceTypeAccess().getFinish2startEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalTextSimplePDL.g:324:2: ( ( 'f2f' ) )
                    {
                    // InternalTextSimplePDL.g:324:2: ( ( 'f2f' ) )
                    // InternalTextSimplePDL.g:325:3: ( 'f2f' )
                    {
                     before(grammarAccess.getWorkSequenceTypeAccess().getFinish2finishEnumLiteralDeclaration_3()); 
                    // InternalTextSimplePDL.g:326:3: ( 'f2f' )
                    // InternalTextSimplePDL.g:326:4: 'f2f'
                    {
                    match(input,14,FOLLOW_2); 

                    }

                     after(grammarAccess.getWorkSequenceTypeAccess().getFinish2finishEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSequenceType__Alternatives"


    // $ANTLR start "rule__Process__Group__0"
    // InternalTextSimplePDL.g:334:1: rule__Process__Group__0 : rule__Process__Group__0__Impl rule__Process__Group__1 ;
    public final void rule__Process__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:338:1: ( rule__Process__Group__0__Impl rule__Process__Group__1 )
            // InternalTextSimplePDL.g:339:2: rule__Process__Group__0__Impl rule__Process__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Process__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Process__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__Group__0"


    // $ANTLR start "rule__Process__Group__0__Impl"
    // InternalTextSimplePDL.g:346:1: rule__Process__Group__0__Impl : ( () ) ;
    public final void rule__Process__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:350:1: ( ( () ) )
            // InternalTextSimplePDL.g:351:1: ( () )
            {
            // InternalTextSimplePDL.g:351:1: ( () )
            // InternalTextSimplePDL.g:352:2: ()
            {
             before(grammarAccess.getProcessAccess().getProcessAction_0()); 
            // InternalTextSimplePDL.g:353:2: ()
            // InternalTextSimplePDL.g:353:3: 
            {
            }

             after(grammarAccess.getProcessAccess().getProcessAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__Group__0__Impl"


    // $ANTLR start "rule__Process__Group__1"
    // InternalTextSimplePDL.g:361:1: rule__Process__Group__1 : rule__Process__Group__1__Impl rule__Process__Group__2 ;
    public final void rule__Process__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:365:1: ( rule__Process__Group__1__Impl rule__Process__Group__2 )
            // InternalTextSimplePDL.g:366:2: rule__Process__Group__1__Impl rule__Process__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Process__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Process__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__Group__1"


    // $ANTLR start "rule__Process__Group__1__Impl"
    // InternalTextSimplePDL.g:373:1: rule__Process__Group__1__Impl : ( 'process' ) ;
    public final void rule__Process__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:377:1: ( ( 'process' ) )
            // InternalTextSimplePDL.g:378:1: ( 'process' )
            {
            // InternalTextSimplePDL.g:378:1: ( 'process' )
            // InternalTextSimplePDL.g:379:2: 'process'
            {
             before(grammarAccess.getProcessAccess().getProcessKeyword_1()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getProcessAccess().getProcessKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__Group__1__Impl"


    // $ANTLR start "rule__Process__Group__2"
    // InternalTextSimplePDL.g:388:1: rule__Process__Group__2 : rule__Process__Group__2__Impl rule__Process__Group__3 ;
    public final void rule__Process__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:392:1: ( rule__Process__Group__2__Impl rule__Process__Group__3 )
            // InternalTextSimplePDL.g:393:2: rule__Process__Group__2__Impl rule__Process__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Process__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Process__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__Group__2"


    // $ANTLR start "rule__Process__Group__2__Impl"
    // InternalTextSimplePDL.g:400:1: rule__Process__Group__2__Impl : ( ( rule__Process__NameAssignment_2 ) ) ;
    public final void rule__Process__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:404:1: ( ( ( rule__Process__NameAssignment_2 ) ) )
            // InternalTextSimplePDL.g:405:1: ( ( rule__Process__NameAssignment_2 ) )
            {
            // InternalTextSimplePDL.g:405:1: ( ( rule__Process__NameAssignment_2 ) )
            // InternalTextSimplePDL.g:406:2: ( rule__Process__NameAssignment_2 )
            {
             before(grammarAccess.getProcessAccess().getNameAssignment_2()); 
            // InternalTextSimplePDL.g:407:2: ( rule__Process__NameAssignment_2 )
            // InternalTextSimplePDL.g:407:3: rule__Process__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Process__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getProcessAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__Group__2__Impl"


    // $ANTLR start "rule__Process__Group__3"
    // InternalTextSimplePDL.g:415:1: rule__Process__Group__3 : rule__Process__Group__3__Impl rule__Process__Group__4 ;
    public final void rule__Process__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:419:1: ( rule__Process__Group__3__Impl rule__Process__Group__4 )
            // InternalTextSimplePDL.g:420:2: rule__Process__Group__3__Impl rule__Process__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__Process__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Process__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__Group__3"


    // $ANTLR start "rule__Process__Group__3__Impl"
    // InternalTextSimplePDL.g:427:1: rule__Process__Group__3__Impl : ( '{' ) ;
    public final void rule__Process__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:431:1: ( ( '{' ) )
            // InternalTextSimplePDL.g:432:1: ( '{' )
            {
            // InternalTextSimplePDL.g:432:1: ( '{' )
            // InternalTextSimplePDL.g:433:2: '{'
            {
             before(grammarAccess.getProcessAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getProcessAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__Group__3__Impl"


    // $ANTLR start "rule__Process__Group__4"
    // InternalTextSimplePDL.g:442:1: rule__Process__Group__4 : rule__Process__Group__4__Impl rule__Process__Group__5 ;
    public final void rule__Process__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:446:1: ( rule__Process__Group__4__Impl rule__Process__Group__5 )
            // InternalTextSimplePDL.g:447:2: rule__Process__Group__4__Impl rule__Process__Group__5
            {
            pushFollow(FOLLOW_6);
            rule__Process__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Process__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__Group__4"


    // $ANTLR start "rule__Process__Group__4__Impl"
    // InternalTextSimplePDL.g:454:1: rule__Process__Group__4__Impl : ( ( rule__Process__ProcessElementsAssignment_4 )* ) ;
    public final void rule__Process__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:458:1: ( ( ( rule__Process__ProcessElementsAssignment_4 )* ) )
            // InternalTextSimplePDL.g:459:1: ( ( rule__Process__ProcessElementsAssignment_4 )* )
            {
            // InternalTextSimplePDL.g:459:1: ( ( rule__Process__ProcessElementsAssignment_4 )* )
            // InternalTextSimplePDL.g:460:2: ( rule__Process__ProcessElementsAssignment_4 )*
            {
             before(grammarAccess.getProcessAccess().getProcessElementsAssignment_4()); 
            // InternalTextSimplePDL.g:461:2: ( rule__Process__ProcessElementsAssignment_4 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0>=18 && LA3_0<=19)||LA3_0==23||LA3_0==25) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalTextSimplePDL.g:461:3: rule__Process__ProcessElementsAssignment_4
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__Process__ProcessElementsAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getProcessAccess().getProcessElementsAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__Group__4__Impl"


    // $ANTLR start "rule__Process__Group__5"
    // InternalTextSimplePDL.g:469:1: rule__Process__Group__5 : rule__Process__Group__5__Impl ;
    public final void rule__Process__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:473:1: ( rule__Process__Group__5__Impl )
            // InternalTextSimplePDL.g:474:2: rule__Process__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Process__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__Group__5"


    // $ANTLR start "rule__Process__Group__5__Impl"
    // InternalTextSimplePDL.g:480:1: rule__Process__Group__5__Impl : ( '}' ) ;
    public final void rule__Process__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:484:1: ( ( '}' ) )
            // InternalTextSimplePDL.g:485:1: ( '}' )
            {
            // InternalTextSimplePDL.g:485:1: ( '}' )
            // InternalTextSimplePDL.g:486:2: '}'
            {
             before(grammarAccess.getProcessAccess().getRightCurlyBracketKeyword_5()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getProcessAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__Group__5__Impl"


    // $ANTLR start "rule__WorkDefinition__Group__0"
    // InternalTextSimplePDL.g:496:1: rule__WorkDefinition__Group__0 : rule__WorkDefinition__Group__0__Impl rule__WorkDefinition__Group__1 ;
    public final void rule__WorkDefinition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:500:1: ( rule__WorkDefinition__Group__0__Impl rule__WorkDefinition__Group__1 )
            // InternalTextSimplePDL.g:501:2: rule__WorkDefinition__Group__0__Impl rule__WorkDefinition__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__WorkDefinition__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WorkDefinition__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDefinition__Group__0"


    // $ANTLR start "rule__WorkDefinition__Group__0__Impl"
    // InternalTextSimplePDL.g:508:1: rule__WorkDefinition__Group__0__Impl : ( () ) ;
    public final void rule__WorkDefinition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:512:1: ( ( () ) )
            // InternalTextSimplePDL.g:513:1: ( () )
            {
            // InternalTextSimplePDL.g:513:1: ( () )
            // InternalTextSimplePDL.g:514:2: ()
            {
             before(grammarAccess.getWorkDefinitionAccess().getWorkDefinitionAction_0()); 
            // InternalTextSimplePDL.g:515:2: ()
            // InternalTextSimplePDL.g:515:3: 
            {
            }

             after(grammarAccess.getWorkDefinitionAccess().getWorkDefinitionAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDefinition__Group__0__Impl"


    // $ANTLR start "rule__WorkDefinition__Group__1"
    // InternalTextSimplePDL.g:523:1: rule__WorkDefinition__Group__1 : rule__WorkDefinition__Group__1__Impl rule__WorkDefinition__Group__2 ;
    public final void rule__WorkDefinition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:527:1: ( rule__WorkDefinition__Group__1__Impl rule__WorkDefinition__Group__2 )
            // InternalTextSimplePDL.g:528:2: rule__WorkDefinition__Group__1__Impl rule__WorkDefinition__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__WorkDefinition__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WorkDefinition__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDefinition__Group__1"


    // $ANTLR start "rule__WorkDefinition__Group__1__Impl"
    // InternalTextSimplePDL.g:535:1: rule__WorkDefinition__Group__1__Impl : ( 'WorkDefinition' ) ;
    public final void rule__WorkDefinition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:539:1: ( ( 'WorkDefinition' ) )
            // InternalTextSimplePDL.g:540:1: ( 'WorkDefinition' )
            {
            // InternalTextSimplePDL.g:540:1: ( 'WorkDefinition' )
            // InternalTextSimplePDL.g:541:2: 'WorkDefinition'
            {
             before(grammarAccess.getWorkDefinitionAccess().getWorkDefinitionKeyword_1()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getWorkDefinitionAccess().getWorkDefinitionKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDefinition__Group__1__Impl"


    // $ANTLR start "rule__WorkDefinition__Group__2"
    // InternalTextSimplePDL.g:550:1: rule__WorkDefinition__Group__2 : rule__WorkDefinition__Group__2__Impl rule__WorkDefinition__Group__3 ;
    public final void rule__WorkDefinition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:554:1: ( rule__WorkDefinition__Group__2__Impl rule__WorkDefinition__Group__3 )
            // InternalTextSimplePDL.g:555:2: rule__WorkDefinition__Group__2__Impl rule__WorkDefinition__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__WorkDefinition__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WorkDefinition__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDefinition__Group__2"


    // $ANTLR start "rule__WorkDefinition__Group__2__Impl"
    // InternalTextSimplePDL.g:562:1: rule__WorkDefinition__Group__2__Impl : ( ( rule__WorkDefinition__NameAssignment_2 ) ) ;
    public final void rule__WorkDefinition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:566:1: ( ( ( rule__WorkDefinition__NameAssignment_2 ) ) )
            // InternalTextSimplePDL.g:567:1: ( ( rule__WorkDefinition__NameAssignment_2 ) )
            {
            // InternalTextSimplePDL.g:567:1: ( ( rule__WorkDefinition__NameAssignment_2 ) )
            // InternalTextSimplePDL.g:568:2: ( rule__WorkDefinition__NameAssignment_2 )
            {
             before(grammarAccess.getWorkDefinitionAccess().getNameAssignment_2()); 
            // InternalTextSimplePDL.g:569:2: ( rule__WorkDefinition__NameAssignment_2 )
            // InternalTextSimplePDL.g:569:3: rule__WorkDefinition__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__WorkDefinition__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getWorkDefinitionAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDefinition__Group__2__Impl"


    // $ANTLR start "rule__WorkDefinition__Group__3"
    // InternalTextSimplePDL.g:577:1: rule__WorkDefinition__Group__3 : rule__WorkDefinition__Group__3__Impl ;
    public final void rule__WorkDefinition__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:581:1: ( rule__WorkDefinition__Group__3__Impl )
            // InternalTextSimplePDL.g:582:2: rule__WorkDefinition__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__WorkDefinition__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDefinition__Group__3"


    // $ANTLR start "rule__WorkDefinition__Group__3__Impl"
    // InternalTextSimplePDL.g:588:1: rule__WorkDefinition__Group__3__Impl : ( ( rule__WorkDefinition__Group_3__0 )? ) ;
    public final void rule__WorkDefinition__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:592:1: ( ( ( rule__WorkDefinition__Group_3__0 )? ) )
            // InternalTextSimplePDL.g:593:1: ( ( rule__WorkDefinition__Group_3__0 )? )
            {
            // InternalTextSimplePDL.g:593:1: ( ( rule__WorkDefinition__Group_3__0 )? )
            // InternalTextSimplePDL.g:594:2: ( rule__WorkDefinition__Group_3__0 )?
            {
             before(grammarAccess.getWorkDefinitionAccess().getGroup_3()); 
            // InternalTextSimplePDL.g:595:2: ( rule__WorkDefinition__Group_3__0 )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==16) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalTextSimplePDL.g:595:3: rule__WorkDefinition__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__WorkDefinition__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getWorkDefinitionAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDefinition__Group__3__Impl"


    // $ANTLR start "rule__WorkDefinition__Group_3__0"
    // InternalTextSimplePDL.g:604:1: rule__WorkDefinition__Group_3__0 : rule__WorkDefinition__Group_3__0__Impl rule__WorkDefinition__Group_3__1 ;
    public final void rule__WorkDefinition__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:608:1: ( rule__WorkDefinition__Group_3__0__Impl rule__WorkDefinition__Group_3__1 )
            // InternalTextSimplePDL.g:609:2: rule__WorkDefinition__Group_3__0__Impl rule__WorkDefinition__Group_3__1
            {
            pushFollow(FOLLOW_9);
            rule__WorkDefinition__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WorkDefinition__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDefinition__Group_3__0"


    // $ANTLR start "rule__WorkDefinition__Group_3__0__Impl"
    // InternalTextSimplePDL.g:616:1: rule__WorkDefinition__Group_3__0__Impl : ( '{' ) ;
    public final void rule__WorkDefinition__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:620:1: ( ( '{' ) )
            // InternalTextSimplePDL.g:621:1: ( '{' )
            {
            // InternalTextSimplePDL.g:621:1: ( '{' )
            // InternalTextSimplePDL.g:622:2: '{'
            {
             before(grammarAccess.getWorkDefinitionAccess().getLeftCurlyBracketKeyword_3_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getWorkDefinitionAccess().getLeftCurlyBracketKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDefinition__Group_3__0__Impl"


    // $ANTLR start "rule__WorkDefinition__Group_3__1"
    // InternalTextSimplePDL.g:631:1: rule__WorkDefinition__Group_3__1 : rule__WorkDefinition__Group_3__1__Impl rule__WorkDefinition__Group_3__2 ;
    public final void rule__WorkDefinition__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:635:1: ( rule__WorkDefinition__Group_3__1__Impl rule__WorkDefinition__Group_3__2 )
            // InternalTextSimplePDL.g:636:2: rule__WorkDefinition__Group_3__1__Impl rule__WorkDefinition__Group_3__2
            {
            pushFollow(FOLLOW_10);
            rule__WorkDefinition__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WorkDefinition__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDefinition__Group_3__1"


    // $ANTLR start "rule__WorkDefinition__Group_3__1__Impl"
    // InternalTextSimplePDL.g:643:1: rule__WorkDefinition__Group_3__1__Impl : ( ( rule__WorkDefinition__UsedResourcesAssignment_3_1 ) ) ;
    public final void rule__WorkDefinition__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:647:1: ( ( ( rule__WorkDefinition__UsedResourcesAssignment_3_1 ) ) )
            // InternalTextSimplePDL.g:648:1: ( ( rule__WorkDefinition__UsedResourcesAssignment_3_1 ) )
            {
            // InternalTextSimplePDL.g:648:1: ( ( rule__WorkDefinition__UsedResourcesAssignment_3_1 ) )
            // InternalTextSimplePDL.g:649:2: ( rule__WorkDefinition__UsedResourcesAssignment_3_1 )
            {
             before(grammarAccess.getWorkDefinitionAccess().getUsedResourcesAssignment_3_1()); 
            // InternalTextSimplePDL.g:650:2: ( rule__WorkDefinition__UsedResourcesAssignment_3_1 )
            // InternalTextSimplePDL.g:650:3: rule__WorkDefinition__UsedResourcesAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__WorkDefinition__UsedResourcesAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getWorkDefinitionAccess().getUsedResourcesAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDefinition__Group_3__1__Impl"


    // $ANTLR start "rule__WorkDefinition__Group_3__2"
    // InternalTextSimplePDL.g:658:1: rule__WorkDefinition__Group_3__2 : rule__WorkDefinition__Group_3__2__Impl ;
    public final void rule__WorkDefinition__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:662:1: ( rule__WorkDefinition__Group_3__2__Impl )
            // InternalTextSimplePDL.g:663:2: rule__WorkDefinition__Group_3__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__WorkDefinition__Group_3__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDefinition__Group_3__2"


    // $ANTLR start "rule__WorkDefinition__Group_3__2__Impl"
    // InternalTextSimplePDL.g:669:1: rule__WorkDefinition__Group_3__2__Impl : ( '}' ) ;
    public final void rule__WorkDefinition__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:673:1: ( ( '}' ) )
            // InternalTextSimplePDL.g:674:1: ( '}' )
            {
            // InternalTextSimplePDL.g:674:1: ( '}' )
            // InternalTextSimplePDL.g:675:2: '}'
            {
             before(grammarAccess.getWorkDefinitionAccess().getRightCurlyBracketKeyword_3_2()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getWorkDefinitionAccess().getRightCurlyBracketKeyword_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDefinition__Group_3__2__Impl"


    // $ANTLR start "rule__WorkSequence__Group__0"
    // InternalTextSimplePDL.g:685:1: rule__WorkSequence__Group__0 : rule__WorkSequence__Group__0__Impl rule__WorkSequence__Group__1 ;
    public final void rule__WorkSequence__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:689:1: ( rule__WorkSequence__Group__0__Impl rule__WorkSequence__Group__1 )
            // InternalTextSimplePDL.g:690:2: rule__WorkSequence__Group__0__Impl rule__WorkSequence__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__WorkSequence__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WorkSequence__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSequence__Group__0"


    // $ANTLR start "rule__WorkSequence__Group__0__Impl"
    // InternalTextSimplePDL.g:697:1: rule__WorkSequence__Group__0__Impl : ( 'WorkSequence' ) ;
    public final void rule__WorkSequence__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:701:1: ( ( 'WorkSequence' ) )
            // InternalTextSimplePDL.g:702:1: ( 'WorkSequence' )
            {
            // InternalTextSimplePDL.g:702:1: ( 'WorkSequence' )
            // InternalTextSimplePDL.g:703:2: 'WorkSequence'
            {
             before(grammarAccess.getWorkSequenceAccess().getWorkSequenceKeyword_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getWorkSequenceAccess().getWorkSequenceKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSequence__Group__0__Impl"


    // $ANTLR start "rule__WorkSequence__Group__1"
    // InternalTextSimplePDL.g:712:1: rule__WorkSequence__Group__1 : rule__WorkSequence__Group__1__Impl rule__WorkSequence__Group__2 ;
    public final void rule__WorkSequence__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:716:1: ( rule__WorkSequence__Group__1__Impl rule__WorkSequence__Group__2 )
            // InternalTextSimplePDL.g:717:2: rule__WorkSequence__Group__1__Impl rule__WorkSequence__Group__2
            {
            pushFollow(FOLLOW_12);
            rule__WorkSequence__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WorkSequence__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSequence__Group__1"


    // $ANTLR start "rule__WorkSequence__Group__1__Impl"
    // InternalTextSimplePDL.g:724:1: rule__WorkSequence__Group__1__Impl : ( ( rule__WorkSequence__LinkTypeAssignment_1 ) ) ;
    public final void rule__WorkSequence__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:728:1: ( ( ( rule__WorkSequence__LinkTypeAssignment_1 ) ) )
            // InternalTextSimplePDL.g:729:1: ( ( rule__WorkSequence__LinkTypeAssignment_1 ) )
            {
            // InternalTextSimplePDL.g:729:1: ( ( rule__WorkSequence__LinkTypeAssignment_1 ) )
            // InternalTextSimplePDL.g:730:2: ( rule__WorkSequence__LinkTypeAssignment_1 )
            {
             before(grammarAccess.getWorkSequenceAccess().getLinkTypeAssignment_1()); 
            // InternalTextSimplePDL.g:731:2: ( rule__WorkSequence__LinkTypeAssignment_1 )
            // InternalTextSimplePDL.g:731:3: rule__WorkSequence__LinkTypeAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__WorkSequence__LinkTypeAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getWorkSequenceAccess().getLinkTypeAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSequence__Group__1__Impl"


    // $ANTLR start "rule__WorkSequence__Group__2"
    // InternalTextSimplePDL.g:739:1: rule__WorkSequence__Group__2 : rule__WorkSequence__Group__2__Impl rule__WorkSequence__Group__3 ;
    public final void rule__WorkSequence__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:743:1: ( rule__WorkSequence__Group__2__Impl rule__WorkSequence__Group__3 )
            // InternalTextSimplePDL.g:744:2: rule__WorkSequence__Group__2__Impl rule__WorkSequence__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__WorkSequence__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WorkSequence__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSequence__Group__2"


    // $ANTLR start "rule__WorkSequence__Group__2__Impl"
    // InternalTextSimplePDL.g:751:1: rule__WorkSequence__Group__2__Impl : ( 'from' ) ;
    public final void rule__WorkSequence__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:755:1: ( ( 'from' ) )
            // InternalTextSimplePDL.g:756:1: ( 'from' )
            {
            // InternalTextSimplePDL.g:756:1: ( 'from' )
            // InternalTextSimplePDL.g:757:2: 'from'
            {
             before(grammarAccess.getWorkSequenceAccess().getFromKeyword_2()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getWorkSequenceAccess().getFromKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSequence__Group__2__Impl"


    // $ANTLR start "rule__WorkSequence__Group__3"
    // InternalTextSimplePDL.g:766:1: rule__WorkSequence__Group__3 : rule__WorkSequence__Group__3__Impl rule__WorkSequence__Group__4 ;
    public final void rule__WorkSequence__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:770:1: ( rule__WorkSequence__Group__3__Impl rule__WorkSequence__Group__4 )
            // InternalTextSimplePDL.g:771:2: rule__WorkSequence__Group__3__Impl rule__WorkSequence__Group__4
            {
            pushFollow(FOLLOW_13);
            rule__WorkSequence__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WorkSequence__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSequence__Group__3"


    // $ANTLR start "rule__WorkSequence__Group__3__Impl"
    // InternalTextSimplePDL.g:778:1: rule__WorkSequence__Group__3__Impl : ( ( rule__WorkSequence__PredecessorAssignment_3 ) ) ;
    public final void rule__WorkSequence__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:782:1: ( ( ( rule__WorkSequence__PredecessorAssignment_3 ) ) )
            // InternalTextSimplePDL.g:783:1: ( ( rule__WorkSequence__PredecessorAssignment_3 ) )
            {
            // InternalTextSimplePDL.g:783:1: ( ( rule__WorkSequence__PredecessorAssignment_3 ) )
            // InternalTextSimplePDL.g:784:2: ( rule__WorkSequence__PredecessorAssignment_3 )
            {
             before(grammarAccess.getWorkSequenceAccess().getPredecessorAssignment_3()); 
            // InternalTextSimplePDL.g:785:2: ( rule__WorkSequence__PredecessorAssignment_3 )
            // InternalTextSimplePDL.g:785:3: rule__WorkSequence__PredecessorAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__WorkSequence__PredecessorAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getWorkSequenceAccess().getPredecessorAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSequence__Group__3__Impl"


    // $ANTLR start "rule__WorkSequence__Group__4"
    // InternalTextSimplePDL.g:793:1: rule__WorkSequence__Group__4 : rule__WorkSequence__Group__4__Impl rule__WorkSequence__Group__5 ;
    public final void rule__WorkSequence__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:797:1: ( rule__WorkSequence__Group__4__Impl rule__WorkSequence__Group__5 )
            // InternalTextSimplePDL.g:798:2: rule__WorkSequence__Group__4__Impl rule__WorkSequence__Group__5
            {
            pushFollow(FOLLOW_4);
            rule__WorkSequence__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WorkSequence__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSequence__Group__4"


    // $ANTLR start "rule__WorkSequence__Group__4__Impl"
    // InternalTextSimplePDL.g:805:1: rule__WorkSequence__Group__4__Impl : ( 'to' ) ;
    public final void rule__WorkSequence__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:809:1: ( ( 'to' ) )
            // InternalTextSimplePDL.g:810:1: ( 'to' )
            {
            // InternalTextSimplePDL.g:810:1: ( 'to' )
            // InternalTextSimplePDL.g:811:2: 'to'
            {
             before(grammarAccess.getWorkSequenceAccess().getToKeyword_4()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getWorkSequenceAccess().getToKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSequence__Group__4__Impl"


    // $ANTLR start "rule__WorkSequence__Group__5"
    // InternalTextSimplePDL.g:820:1: rule__WorkSequence__Group__5 : rule__WorkSequence__Group__5__Impl rule__WorkSequence__Group__6 ;
    public final void rule__WorkSequence__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:824:1: ( rule__WorkSequence__Group__5__Impl rule__WorkSequence__Group__6 )
            // InternalTextSimplePDL.g:825:2: rule__WorkSequence__Group__5__Impl rule__WorkSequence__Group__6
            {
            pushFollow(FOLLOW_14);
            rule__WorkSequence__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WorkSequence__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSequence__Group__5"


    // $ANTLR start "rule__WorkSequence__Group__5__Impl"
    // InternalTextSimplePDL.g:832:1: rule__WorkSequence__Group__5__Impl : ( ( rule__WorkSequence__SuccessorAssignment_5 ) ) ;
    public final void rule__WorkSequence__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:836:1: ( ( ( rule__WorkSequence__SuccessorAssignment_5 ) ) )
            // InternalTextSimplePDL.g:837:1: ( ( rule__WorkSequence__SuccessorAssignment_5 ) )
            {
            // InternalTextSimplePDL.g:837:1: ( ( rule__WorkSequence__SuccessorAssignment_5 ) )
            // InternalTextSimplePDL.g:838:2: ( rule__WorkSequence__SuccessorAssignment_5 )
            {
             before(grammarAccess.getWorkSequenceAccess().getSuccessorAssignment_5()); 
            // InternalTextSimplePDL.g:839:2: ( rule__WorkSequence__SuccessorAssignment_5 )
            // InternalTextSimplePDL.g:839:3: rule__WorkSequence__SuccessorAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__WorkSequence__SuccessorAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getWorkSequenceAccess().getSuccessorAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSequence__Group__5__Impl"


    // $ANTLR start "rule__WorkSequence__Group__6"
    // InternalTextSimplePDL.g:847:1: rule__WorkSequence__Group__6 : rule__WorkSequence__Group__6__Impl ;
    public final void rule__WorkSequence__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:851:1: ( rule__WorkSequence__Group__6__Impl )
            // InternalTextSimplePDL.g:852:2: rule__WorkSequence__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__WorkSequence__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSequence__Group__6"


    // $ANTLR start "rule__WorkSequence__Group__6__Impl"
    // InternalTextSimplePDL.g:858:1: rule__WorkSequence__Group__6__Impl : ( ';' ) ;
    public final void rule__WorkSequence__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:862:1: ( ( ';' ) )
            // InternalTextSimplePDL.g:863:1: ( ';' )
            {
            // InternalTextSimplePDL.g:863:1: ( ';' )
            // InternalTextSimplePDL.g:864:2: ';'
            {
             before(grammarAccess.getWorkSequenceAccess().getSemicolonKeyword_6()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getWorkSequenceAccess().getSemicolonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSequence__Group__6__Impl"


    // $ANTLR start "rule__Guidance__Group__0"
    // InternalTextSimplePDL.g:874:1: rule__Guidance__Group__0 : rule__Guidance__Group__0__Impl rule__Guidance__Group__1 ;
    public final void rule__Guidance__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:878:1: ( rule__Guidance__Group__0__Impl rule__Guidance__Group__1 )
            // InternalTextSimplePDL.g:879:2: rule__Guidance__Group__0__Impl rule__Guidance__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Guidance__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Guidance__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guidance__Group__0"


    // $ANTLR start "rule__Guidance__Group__0__Impl"
    // InternalTextSimplePDL.g:886:1: rule__Guidance__Group__0__Impl : ( 'Guidance' ) ;
    public final void rule__Guidance__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:890:1: ( ( 'Guidance' ) )
            // InternalTextSimplePDL.g:891:1: ( 'Guidance' )
            {
            // InternalTextSimplePDL.g:891:1: ( 'Guidance' )
            // InternalTextSimplePDL.g:892:2: 'Guidance'
            {
             before(grammarAccess.getGuidanceAccess().getGuidanceKeyword_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getGuidanceAccess().getGuidanceKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guidance__Group__0__Impl"


    // $ANTLR start "rule__Guidance__Group__1"
    // InternalTextSimplePDL.g:901:1: rule__Guidance__Group__1 : rule__Guidance__Group__1__Impl rule__Guidance__Group__2 ;
    public final void rule__Guidance__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:905:1: ( rule__Guidance__Group__1__Impl rule__Guidance__Group__2 )
            // InternalTextSimplePDL.g:906:2: rule__Guidance__Group__1__Impl rule__Guidance__Group__2
            {
            pushFollow(FOLLOW_15);
            rule__Guidance__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Guidance__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guidance__Group__1"


    // $ANTLR start "rule__Guidance__Group__1__Impl"
    // InternalTextSimplePDL.g:913:1: rule__Guidance__Group__1__Impl : ( '{' ) ;
    public final void rule__Guidance__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:917:1: ( ( '{' ) )
            // InternalTextSimplePDL.g:918:1: ( '{' )
            {
            // InternalTextSimplePDL.g:918:1: ( '{' )
            // InternalTextSimplePDL.g:919:2: '{'
            {
             before(grammarAccess.getGuidanceAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getGuidanceAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guidance__Group__1__Impl"


    // $ANTLR start "rule__Guidance__Group__2"
    // InternalTextSimplePDL.g:928:1: rule__Guidance__Group__2 : rule__Guidance__Group__2__Impl rule__Guidance__Group__3 ;
    public final void rule__Guidance__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:932:1: ( rule__Guidance__Group__2__Impl rule__Guidance__Group__3 )
            // InternalTextSimplePDL.g:933:2: rule__Guidance__Group__2__Impl rule__Guidance__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__Guidance__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Guidance__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guidance__Group__2"


    // $ANTLR start "rule__Guidance__Group__2__Impl"
    // InternalTextSimplePDL.g:940:1: rule__Guidance__Group__2__Impl : ( 'description' ) ;
    public final void rule__Guidance__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:944:1: ( ( 'description' ) )
            // InternalTextSimplePDL.g:945:1: ( 'description' )
            {
            // InternalTextSimplePDL.g:945:1: ( 'description' )
            // InternalTextSimplePDL.g:946:2: 'description'
            {
             before(grammarAccess.getGuidanceAccess().getDescriptionKeyword_2()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getGuidanceAccess().getDescriptionKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guidance__Group__2__Impl"


    // $ANTLR start "rule__Guidance__Group__3"
    // InternalTextSimplePDL.g:955:1: rule__Guidance__Group__3 : rule__Guidance__Group__3__Impl rule__Guidance__Group__4 ;
    public final void rule__Guidance__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:959:1: ( rule__Guidance__Group__3__Impl rule__Guidance__Group__4 )
            // InternalTextSimplePDL.g:960:2: rule__Guidance__Group__3__Impl rule__Guidance__Group__4
            {
            pushFollow(FOLLOW_10);
            rule__Guidance__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Guidance__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guidance__Group__3"


    // $ANTLR start "rule__Guidance__Group__3__Impl"
    // InternalTextSimplePDL.g:967:1: rule__Guidance__Group__3__Impl : ( ( rule__Guidance__DescriptionAssignment_3 ) ) ;
    public final void rule__Guidance__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:971:1: ( ( ( rule__Guidance__DescriptionAssignment_3 ) ) )
            // InternalTextSimplePDL.g:972:1: ( ( rule__Guidance__DescriptionAssignment_3 ) )
            {
            // InternalTextSimplePDL.g:972:1: ( ( rule__Guidance__DescriptionAssignment_3 ) )
            // InternalTextSimplePDL.g:973:2: ( rule__Guidance__DescriptionAssignment_3 )
            {
             before(grammarAccess.getGuidanceAccess().getDescriptionAssignment_3()); 
            // InternalTextSimplePDL.g:974:2: ( rule__Guidance__DescriptionAssignment_3 )
            // InternalTextSimplePDL.g:974:3: rule__Guidance__DescriptionAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Guidance__DescriptionAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getGuidanceAccess().getDescriptionAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guidance__Group__3__Impl"


    // $ANTLR start "rule__Guidance__Group__4"
    // InternalTextSimplePDL.g:982:1: rule__Guidance__Group__4 : rule__Guidance__Group__4__Impl ;
    public final void rule__Guidance__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:986:1: ( rule__Guidance__Group__4__Impl )
            // InternalTextSimplePDL.g:987:2: rule__Guidance__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Guidance__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guidance__Group__4"


    // $ANTLR start "rule__Guidance__Group__4__Impl"
    // InternalTextSimplePDL.g:993:1: rule__Guidance__Group__4__Impl : ( '}' ) ;
    public final void rule__Guidance__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:997:1: ( ( '}' ) )
            // InternalTextSimplePDL.g:998:1: ( '}' )
            {
            // InternalTextSimplePDL.g:998:1: ( '}' )
            // InternalTextSimplePDL.g:999:2: '}'
            {
             before(grammarAccess.getGuidanceAccess().getRightCurlyBracketKeyword_4()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getGuidanceAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guidance__Group__4__Impl"


    // $ANTLR start "rule__Resources__Group__0"
    // InternalTextSimplePDL.g:1009:1: rule__Resources__Group__0 : rule__Resources__Group__0__Impl rule__Resources__Group__1 ;
    public final void rule__Resources__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1013:1: ( rule__Resources__Group__0__Impl rule__Resources__Group__1 )
            // InternalTextSimplePDL.g:1014:2: rule__Resources__Group__0__Impl rule__Resources__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Resources__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Resources__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Resources__Group__0"


    // $ANTLR start "rule__Resources__Group__0__Impl"
    // InternalTextSimplePDL.g:1021:1: rule__Resources__Group__0__Impl : ( 'Resources' ) ;
    public final void rule__Resources__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1025:1: ( ( 'Resources' ) )
            // InternalTextSimplePDL.g:1026:1: ( 'Resources' )
            {
            // InternalTextSimplePDL.g:1026:1: ( 'Resources' )
            // InternalTextSimplePDL.g:1027:2: 'Resources'
            {
             before(grammarAccess.getResourcesAccess().getResourcesKeyword_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getResourcesAccess().getResourcesKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Resources__Group__0__Impl"


    // $ANTLR start "rule__Resources__Group__1"
    // InternalTextSimplePDL.g:1036:1: rule__Resources__Group__1 : rule__Resources__Group__1__Impl rule__Resources__Group__2 ;
    public final void rule__Resources__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1040:1: ( rule__Resources__Group__1__Impl rule__Resources__Group__2 )
            // InternalTextSimplePDL.g:1041:2: rule__Resources__Group__1__Impl rule__Resources__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__Resources__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Resources__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Resources__Group__1"


    // $ANTLR start "rule__Resources__Group__1__Impl"
    // InternalTextSimplePDL.g:1048:1: rule__Resources__Group__1__Impl : ( ( rule__Resources__NameAssignment_1 ) ) ;
    public final void rule__Resources__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1052:1: ( ( ( rule__Resources__NameAssignment_1 ) ) )
            // InternalTextSimplePDL.g:1053:1: ( ( rule__Resources__NameAssignment_1 ) )
            {
            // InternalTextSimplePDL.g:1053:1: ( ( rule__Resources__NameAssignment_1 ) )
            // InternalTextSimplePDL.g:1054:2: ( rule__Resources__NameAssignment_1 )
            {
             before(grammarAccess.getResourcesAccess().getNameAssignment_1()); 
            // InternalTextSimplePDL.g:1055:2: ( rule__Resources__NameAssignment_1 )
            // InternalTextSimplePDL.g:1055:3: rule__Resources__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Resources__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getResourcesAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Resources__Group__1__Impl"


    // $ANTLR start "rule__Resources__Group__2"
    // InternalTextSimplePDL.g:1063:1: rule__Resources__Group__2 : rule__Resources__Group__2__Impl ;
    public final void rule__Resources__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1067:1: ( rule__Resources__Group__2__Impl )
            // InternalTextSimplePDL.g:1068:2: rule__Resources__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Resources__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Resources__Group__2"


    // $ANTLR start "rule__Resources__Group__2__Impl"
    // InternalTextSimplePDL.g:1074:1: rule__Resources__Group__2__Impl : ( ( rule__Resources__QuantityAssignment_2 ) ) ;
    public final void rule__Resources__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1078:1: ( ( ( rule__Resources__QuantityAssignment_2 ) ) )
            // InternalTextSimplePDL.g:1079:1: ( ( rule__Resources__QuantityAssignment_2 ) )
            {
            // InternalTextSimplePDL.g:1079:1: ( ( rule__Resources__QuantityAssignment_2 ) )
            // InternalTextSimplePDL.g:1080:2: ( rule__Resources__QuantityAssignment_2 )
            {
             before(grammarAccess.getResourcesAccess().getQuantityAssignment_2()); 
            // InternalTextSimplePDL.g:1081:2: ( rule__Resources__QuantityAssignment_2 )
            // InternalTextSimplePDL.g:1081:3: rule__Resources__QuantityAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Resources__QuantityAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getResourcesAccess().getQuantityAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Resources__Group__2__Impl"


    // $ANTLR start "rule__UsedResources__Group__0"
    // InternalTextSimplePDL.g:1090:1: rule__UsedResources__Group__0 : rule__UsedResources__Group__0__Impl rule__UsedResources__Group__1 ;
    public final void rule__UsedResources__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1094:1: ( rule__UsedResources__Group__0__Impl rule__UsedResources__Group__1 )
            // InternalTextSimplePDL.g:1095:2: rule__UsedResources__Group__0__Impl rule__UsedResources__Group__1
            {
            pushFollow(FOLLOW_16);
            rule__UsedResources__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UsedResources__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UsedResources__Group__0"


    // $ANTLR start "rule__UsedResources__Group__0__Impl"
    // InternalTextSimplePDL.g:1102:1: rule__UsedResources__Group__0__Impl : ( 'uses' ) ;
    public final void rule__UsedResources__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1106:1: ( ( 'uses' ) )
            // InternalTextSimplePDL.g:1107:1: ( 'uses' )
            {
            // InternalTextSimplePDL.g:1107:1: ( 'uses' )
            // InternalTextSimplePDL.g:1108:2: 'uses'
            {
             before(grammarAccess.getUsedResourcesAccess().getUsesKeyword_0()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getUsedResourcesAccess().getUsesKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UsedResources__Group__0__Impl"


    // $ANTLR start "rule__UsedResources__Group__1"
    // InternalTextSimplePDL.g:1117:1: rule__UsedResources__Group__1 : rule__UsedResources__Group__1__Impl rule__UsedResources__Group__2 ;
    public final void rule__UsedResources__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1121:1: ( rule__UsedResources__Group__1__Impl rule__UsedResources__Group__2 )
            // InternalTextSimplePDL.g:1122:2: rule__UsedResources__Group__1__Impl rule__UsedResources__Group__2
            {
            pushFollow(FOLLOW_17);
            rule__UsedResources__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UsedResources__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UsedResources__Group__1"


    // $ANTLR start "rule__UsedResources__Group__1__Impl"
    // InternalTextSimplePDL.g:1129:1: rule__UsedResources__Group__1__Impl : ( ( rule__UsedResources__QuantityAssignment_1 ) ) ;
    public final void rule__UsedResources__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1133:1: ( ( ( rule__UsedResources__QuantityAssignment_1 ) ) )
            // InternalTextSimplePDL.g:1134:1: ( ( rule__UsedResources__QuantityAssignment_1 ) )
            {
            // InternalTextSimplePDL.g:1134:1: ( ( rule__UsedResources__QuantityAssignment_1 ) )
            // InternalTextSimplePDL.g:1135:2: ( rule__UsedResources__QuantityAssignment_1 )
            {
             before(grammarAccess.getUsedResourcesAccess().getQuantityAssignment_1()); 
            // InternalTextSimplePDL.g:1136:2: ( rule__UsedResources__QuantityAssignment_1 )
            // InternalTextSimplePDL.g:1136:3: rule__UsedResources__QuantityAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__UsedResources__QuantityAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getUsedResourcesAccess().getQuantityAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UsedResources__Group__1__Impl"


    // $ANTLR start "rule__UsedResources__Group__2"
    // InternalTextSimplePDL.g:1144:1: rule__UsedResources__Group__2 : rule__UsedResources__Group__2__Impl rule__UsedResources__Group__3 ;
    public final void rule__UsedResources__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1148:1: ( rule__UsedResources__Group__2__Impl rule__UsedResources__Group__3 )
            // InternalTextSimplePDL.g:1149:2: rule__UsedResources__Group__2__Impl rule__UsedResources__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__UsedResources__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UsedResources__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UsedResources__Group__2"


    // $ANTLR start "rule__UsedResources__Group__2__Impl"
    // InternalTextSimplePDL.g:1156:1: rule__UsedResources__Group__2__Impl : ( 'of' ) ;
    public final void rule__UsedResources__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1160:1: ( ( 'of' ) )
            // InternalTextSimplePDL.g:1161:1: ( 'of' )
            {
            // InternalTextSimplePDL.g:1161:1: ( 'of' )
            // InternalTextSimplePDL.g:1162:2: 'of'
            {
             before(grammarAccess.getUsedResourcesAccess().getOfKeyword_2()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getUsedResourcesAccess().getOfKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UsedResources__Group__2__Impl"


    // $ANTLR start "rule__UsedResources__Group__3"
    // InternalTextSimplePDL.g:1171:1: rule__UsedResources__Group__3 : rule__UsedResources__Group__3__Impl rule__UsedResources__Group__4 ;
    public final void rule__UsedResources__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1175:1: ( rule__UsedResources__Group__3__Impl rule__UsedResources__Group__4 )
            // InternalTextSimplePDL.g:1176:2: rule__UsedResources__Group__3__Impl rule__UsedResources__Group__4
            {
            pushFollow(FOLLOW_14);
            rule__UsedResources__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UsedResources__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UsedResources__Group__3"


    // $ANTLR start "rule__UsedResources__Group__3__Impl"
    // InternalTextSimplePDL.g:1183:1: rule__UsedResources__Group__3__Impl : ( ( rule__UsedResources__RsrcesAssignment_3 ) ) ;
    public final void rule__UsedResources__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1187:1: ( ( ( rule__UsedResources__RsrcesAssignment_3 ) ) )
            // InternalTextSimplePDL.g:1188:1: ( ( rule__UsedResources__RsrcesAssignment_3 ) )
            {
            // InternalTextSimplePDL.g:1188:1: ( ( rule__UsedResources__RsrcesAssignment_3 ) )
            // InternalTextSimplePDL.g:1189:2: ( rule__UsedResources__RsrcesAssignment_3 )
            {
             before(grammarAccess.getUsedResourcesAccess().getRsrcesAssignment_3()); 
            // InternalTextSimplePDL.g:1190:2: ( rule__UsedResources__RsrcesAssignment_3 )
            // InternalTextSimplePDL.g:1190:3: rule__UsedResources__RsrcesAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__UsedResources__RsrcesAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getUsedResourcesAccess().getRsrcesAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UsedResources__Group__3__Impl"


    // $ANTLR start "rule__UsedResources__Group__4"
    // InternalTextSimplePDL.g:1198:1: rule__UsedResources__Group__4 : rule__UsedResources__Group__4__Impl ;
    public final void rule__UsedResources__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1202:1: ( rule__UsedResources__Group__4__Impl )
            // InternalTextSimplePDL.g:1203:2: rule__UsedResources__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UsedResources__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UsedResources__Group__4"


    // $ANTLR start "rule__UsedResources__Group__4__Impl"
    // InternalTextSimplePDL.g:1209:1: rule__UsedResources__Group__4__Impl : ( ';' ) ;
    public final void rule__UsedResources__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1213:1: ( ( ';' ) )
            // InternalTextSimplePDL.g:1214:1: ( ';' )
            {
            // InternalTextSimplePDL.g:1214:1: ( ';' )
            // InternalTextSimplePDL.g:1215:2: ';'
            {
             before(grammarAccess.getUsedResourcesAccess().getSemicolonKeyword_4()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getUsedResourcesAccess().getSemicolonKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UsedResources__Group__4__Impl"


    // $ANTLR start "rule__EInt__Group__0"
    // InternalTextSimplePDL.g:1225:1: rule__EInt__Group__0 : rule__EInt__Group__0__Impl rule__EInt__Group__1 ;
    public final void rule__EInt__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1229:1: ( rule__EInt__Group__0__Impl rule__EInt__Group__1 )
            // InternalTextSimplePDL.g:1230:2: rule__EInt__Group__0__Impl rule__EInt__Group__1
            {
            pushFollow(FOLLOW_16);
            rule__EInt__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EInt__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__0"


    // $ANTLR start "rule__EInt__Group__0__Impl"
    // InternalTextSimplePDL.g:1237:1: rule__EInt__Group__0__Impl : ( ( '-' )? ) ;
    public final void rule__EInt__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1241:1: ( ( ( '-' )? ) )
            // InternalTextSimplePDL.g:1242:1: ( ( '-' )? )
            {
            // InternalTextSimplePDL.g:1242:1: ( ( '-' )? )
            // InternalTextSimplePDL.g:1243:2: ( '-' )?
            {
             before(grammarAccess.getEIntAccess().getHyphenMinusKeyword_0()); 
            // InternalTextSimplePDL.g:1244:2: ( '-' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==28) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalTextSimplePDL.g:1244:3: '-'
                    {
                    match(input,28,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getEIntAccess().getHyphenMinusKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__0__Impl"


    // $ANTLR start "rule__EInt__Group__1"
    // InternalTextSimplePDL.g:1252:1: rule__EInt__Group__1 : rule__EInt__Group__1__Impl ;
    public final void rule__EInt__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1256:1: ( rule__EInt__Group__1__Impl )
            // InternalTextSimplePDL.g:1257:2: rule__EInt__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EInt__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__1"


    // $ANTLR start "rule__EInt__Group__1__Impl"
    // InternalTextSimplePDL.g:1263:1: rule__EInt__Group__1__Impl : ( RULE_INT ) ;
    public final void rule__EInt__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1267:1: ( ( RULE_INT ) )
            // InternalTextSimplePDL.g:1268:1: ( RULE_INT )
            {
            // InternalTextSimplePDL.g:1268:1: ( RULE_INT )
            // InternalTextSimplePDL.g:1269:2: RULE_INT
            {
             before(grammarAccess.getEIntAccess().getINTTerminalRuleCall_1()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getEIntAccess().getINTTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__1__Impl"


    // $ANTLR start "rule__Process__NameAssignment_2"
    // InternalTextSimplePDL.g:1279:1: rule__Process__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__Process__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1283:1: ( ( RULE_ID ) )
            // InternalTextSimplePDL.g:1284:2: ( RULE_ID )
            {
            // InternalTextSimplePDL.g:1284:2: ( RULE_ID )
            // InternalTextSimplePDL.g:1285:3: RULE_ID
            {
             before(grammarAccess.getProcessAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getProcessAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__NameAssignment_2"


    // $ANTLR start "rule__Process__ProcessElementsAssignment_4"
    // InternalTextSimplePDL.g:1294:1: rule__Process__ProcessElementsAssignment_4 : ( ruleProcessElement ) ;
    public final void rule__Process__ProcessElementsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1298:1: ( ( ruleProcessElement ) )
            // InternalTextSimplePDL.g:1299:2: ( ruleProcessElement )
            {
            // InternalTextSimplePDL.g:1299:2: ( ruleProcessElement )
            // InternalTextSimplePDL.g:1300:3: ruleProcessElement
            {
             before(grammarAccess.getProcessAccess().getProcessElementsProcessElementParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleProcessElement();

            state._fsp--;

             after(grammarAccess.getProcessAccess().getProcessElementsProcessElementParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Process__ProcessElementsAssignment_4"


    // $ANTLR start "rule__WorkDefinition__NameAssignment_2"
    // InternalTextSimplePDL.g:1309:1: rule__WorkDefinition__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__WorkDefinition__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1313:1: ( ( RULE_ID ) )
            // InternalTextSimplePDL.g:1314:2: ( RULE_ID )
            {
            // InternalTextSimplePDL.g:1314:2: ( RULE_ID )
            // InternalTextSimplePDL.g:1315:3: RULE_ID
            {
             before(grammarAccess.getWorkDefinitionAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getWorkDefinitionAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDefinition__NameAssignment_2"


    // $ANTLR start "rule__WorkDefinition__UsedResourcesAssignment_3_1"
    // InternalTextSimplePDL.g:1324:1: rule__WorkDefinition__UsedResourcesAssignment_3_1 : ( ruleUsedResources ) ;
    public final void rule__WorkDefinition__UsedResourcesAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1328:1: ( ( ruleUsedResources ) )
            // InternalTextSimplePDL.g:1329:2: ( ruleUsedResources )
            {
            // InternalTextSimplePDL.g:1329:2: ( ruleUsedResources )
            // InternalTextSimplePDL.g:1330:3: ruleUsedResources
            {
             before(grammarAccess.getWorkDefinitionAccess().getUsedResourcesUsedResourcesParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleUsedResources();

            state._fsp--;

             after(grammarAccess.getWorkDefinitionAccess().getUsedResourcesUsedResourcesParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkDefinition__UsedResourcesAssignment_3_1"


    // $ANTLR start "rule__WorkSequence__LinkTypeAssignment_1"
    // InternalTextSimplePDL.g:1339:1: rule__WorkSequence__LinkTypeAssignment_1 : ( ruleWorkSequenceType ) ;
    public final void rule__WorkSequence__LinkTypeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1343:1: ( ( ruleWorkSequenceType ) )
            // InternalTextSimplePDL.g:1344:2: ( ruleWorkSequenceType )
            {
            // InternalTextSimplePDL.g:1344:2: ( ruleWorkSequenceType )
            // InternalTextSimplePDL.g:1345:3: ruleWorkSequenceType
            {
             before(grammarAccess.getWorkSequenceAccess().getLinkTypeWorkSequenceTypeEnumRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleWorkSequenceType();

            state._fsp--;

             after(grammarAccess.getWorkSequenceAccess().getLinkTypeWorkSequenceTypeEnumRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSequence__LinkTypeAssignment_1"


    // $ANTLR start "rule__WorkSequence__PredecessorAssignment_3"
    // InternalTextSimplePDL.g:1354:1: rule__WorkSequence__PredecessorAssignment_3 : ( ( RULE_ID ) ) ;
    public final void rule__WorkSequence__PredecessorAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1358:1: ( ( ( RULE_ID ) ) )
            // InternalTextSimplePDL.g:1359:2: ( ( RULE_ID ) )
            {
            // InternalTextSimplePDL.g:1359:2: ( ( RULE_ID ) )
            // InternalTextSimplePDL.g:1360:3: ( RULE_ID )
            {
             before(grammarAccess.getWorkSequenceAccess().getPredecessorWorkDefinitionCrossReference_3_0()); 
            // InternalTextSimplePDL.g:1361:3: ( RULE_ID )
            // InternalTextSimplePDL.g:1362:4: RULE_ID
            {
             before(grammarAccess.getWorkSequenceAccess().getPredecessorWorkDefinitionIDTerminalRuleCall_3_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getWorkSequenceAccess().getPredecessorWorkDefinitionIDTerminalRuleCall_3_0_1()); 

            }

             after(grammarAccess.getWorkSequenceAccess().getPredecessorWorkDefinitionCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSequence__PredecessorAssignment_3"


    // $ANTLR start "rule__WorkSequence__SuccessorAssignment_5"
    // InternalTextSimplePDL.g:1373:1: rule__WorkSequence__SuccessorAssignment_5 : ( ( RULE_ID ) ) ;
    public final void rule__WorkSequence__SuccessorAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1377:1: ( ( ( RULE_ID ) ) )
            // InternalTextSimplePDL.g:1378:2: ( ( RULE_ID ) )
            {
            // InternalTextSimplePDL.g:1378:2: ( ( RULE_ID ) )
            // InternalTextSimplePDL.g:1379:3: ( RULE_ID )
            {
             before(grammarAccess.getWorkSequenceAccess().getSuccessorWorkDefinitionCrossReference_5_0()); 
            // InternalTextSimplePDL.g:1380:3: ( RULE_ID )
            // InternalTextSimplePDL.g:1381:4: RULE_ID
            {
             before(grammarAccess.getWorkSequenceAccess().getSuccessorWorkDefinitionIDTerminalRuleCall_5_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getWorkSequenceAccess().getSuccessorWorkDefinitionIDTerminalRuleCall_5_0_1()); 

            }

             after(grammarAccess.getWorkSequenceAccess().getSuccessorWorkDefinitionCrossReference_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WorkSequence__SuccessorAssignment_5"


    // $ANTLR start "rule__Guidance__DescriptionAssignment_3"
    // InternalTextSimplePDL.g:1392:1: rule__Guidance__DescriptionAssignment_3 : ( RULE_ID ) ;
    public final void rule__Guidance__DescriptionAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1396:1: ( ( RULE_ID ) )
            // InternalTextSimplePDL.g:1397:2: ( RULE_ID )
            {
            // InternalTextSimplePDL.g:1397:2: ( RULE_ID )
            // InternalTextSimplePDL.g:1398:3: RULE_ID
            {
             before(grammarAccess.getGuidanceAccess().getDescriptionIDTerminalRuleCall_3_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getGuidanceAccess().getDescriptionIDTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guidance__DescriptionAssignment_3"


    // $ANTLR start "rule__Resources__NameAssignment_1"
    // InternalTextSimplePDL.g:1407:1: rule__Resources__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Resources__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1411:1: ( ( RULE_ID ) )
            // InternalTextSimplePDL.g:1412:2: ( RULE_ID )
            {
            // InternalTextSimplePDL.g:1412:2: ( RULE_ID )
            // InternalTextSimplePDL.g:1413:3: RULE_ID
            {
             before(grammarAccess.getResourcesAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getResourcesAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Resources__NameAssignment_1"


    // $ANTLR start "rule__Resources__QuantityAssignment_2"
    // InternalTextSimplePDL.g:1422:1: rule__Resources__QuantityAssignment_2 : ( ruleEInt ) ;
    public final void rule__Resources__QuantityAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1426:1: ( ( ruleEInt ) )
            // InternalTextSimplePDL.g:1427:2: ( ruleEInt )
            {
            // InternalTextSimplePDL.g:1427:2: ( ruleEInt )
            // InternalTextSimplePDL.g:1428:3: ruleEInt
            {
             before(grammarAccess.getResourcesAccess().getQuantityEIntParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getResourcesAccess().getQuantityEIntParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Resources__QuantityAssignment_2"


    // $ANTLR start "rule__UsedResources__QuantityAssignment_1"
    // InternalTextSimplePDL.g:1437:1: rule__UsedResources__QuantityAssignment_1 : ( ruleEInt ) ;
    public final void rule__UsedResources__QuantityAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1441:1: ( ( ruleEInt ) )
            // InternalTextSimplePDL.g:1442:2: ( ruleEInt )
            {
            // InternalTextSimplePDL.g:1442:2: ( ruleEInt )
            // InternalTextSimplePDL.g:1443:3: ruleEInt
            {
             before(grammarAccess.getUsedResourcesAccess().getQuantityEIntParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getUsedResourcesAccess().getQuantityEIntParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UsedResources__QuantityAssignment_1"


    // $ANTLR start "rule__UsedResources__RsrcesAssignment_3"
    // InternalTextSimplePDL.g:1452:1: rule__UsedResources__RsrcesAssignment_3 : ( ( RULE_ID ) ) ;
    public final void rule__UsedResources__RsrcesAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTextSimplePDL.g:1456:1: ( ( ( RULE_ID ) ) )
            // InternalTextSimplePDL.g:1457:2: ( ( RULE_ID ) )
            {
            // InternalTextSimplePDL.g:1457:2: ( ( RULE_ID ) )
            // InternalTextSimplePDL.g:1458:3: ( RULE_ID )
            {
             before(grammarAccess.getUsedResourcesAccess().getRsrcesResourcesCrossReference_3_0()); 
            // InternalTextSimplePDL.g:1459:3: ( RULE_ID )
            // InternalTextSimplePDL.g:1460:4: RULE_ID
            {
             before(grammarAccess.getUsedResourcesAccess().getRsrcesResourcesIDTerminalRuleCall_3_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getUsedResourcesAccess().getRsrcesResourcesIDTerminalRuleCall_3_0_1()); 

            }

             after(grammarAccess.getUsedResourcesAccess().getRsrcesResourcesCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UsedResources__RsrcesAssignment_3"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x00000000028E0000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x00000000028C0002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000007800L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000010000010L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000008000000L});

}