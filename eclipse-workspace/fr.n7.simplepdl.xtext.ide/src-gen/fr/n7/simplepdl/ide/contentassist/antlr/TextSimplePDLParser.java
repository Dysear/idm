/*
 * generated by Xtext 2.29.0
 */
package fr.n7.simplepdl.ide.contentassist.antlr;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import fr.n7.simplepdl.ide.contentassist.antlr.internal.InternalTextSimplePDLParser;
import fr.n7.simplepdl.services.TextSimplePDLGrammarAccess;
import java.util.Map;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.ide.editor.contentassist.antlr.AbstractContentAssistParser;

public class TextSimplePDLParser extends AbstractContentAssistParser {

	@Singleton
	public static final class NameMappings {
		
		private final Map<AbstractElement, String> mappings;
		
		@Inject
		public NameMappings(TextSimplePDLGrammarAccess grammarAccess) {
			ImmutableMap.Builder<AbstractElement, String> builder = ImmutableMap.builder();
			init(builder, grammarAccess);
			this.mappings = builder.build();
		}
		
		public String getRuleName(AbstractElement element) {
			return mappings.get(element);
		}
		
		private static void init(ImmutableMap.Builder<AbstractElement, String> builder, TextSimplePDLGrammarAccess grammarAccess) {
			builder.put(grammarAccess.getProcessElementAccess().getAlternatives(), "rule__ProcessElement__Alternatives");
			builder.put(grammarAccess.getWorkSequenceTypeAccess().getAlternatives(), "rule__WorkSequenceType__Alternatives");
			builder.put(grammarAccess.getProcessAccess().getGroup(), "rule__Process__Group__0");
			builder.put(grammarAccess.getWorkDefinitionAccess().getGroup(), "rule__WorkDefinition__Group__0");
			builder.put(grammarAccess.getWorkDefinitionAccess().getGroup_3(), "rule__WorkDefinition__Group_3__0");
			builder.put(grammarAccess.getWorkSequenceAccess().getGroup(), "rule__WorkSequence__Group__0");
			builder.put(grammarAccess.getGuidanceAccess().getGroup(), "rule__Guidance__Group__0");
			builder.put(grammarAccess.getResourcesAccess().getGroup(), "rule__Resources__Group__0");
			builder.put(grammarAccess.getUsedResourcesAccess().getGroup(), "rule__UsedResources__Group__0");
			builder.put(grammarAccess.getEIntAccess().getGroup(), "rule__EInt__Group__0");
			builder.put(grammarAccess.getProcessAccess().getNameAssignment_2(), "rule__Process__NameAssignment_2");
			builder.put(grammarAccess.getProcessAccess().getProcessElementsAssignment_4(), "rule__Process__ProcessElementsAssignment_4");
			builder.put(grammarAccess.getWorkDefinitionAccess().getNameAssignment_2(), "rule__WorkDefinition__NameAssignment_2");
			builder.put(grammarAccess.getWorkDefinitionAccess().getUsedResourcesAssignment_3_1(), "rule__WorkDefinition__UsedResourcesAssignment_3_1");
			builder.put(grammarAccess.getWorkSequenceAccess().getLinkTypeAssignment_1(), "rule__WorkSequence__LinkTypeAssignment_1");
			builder.put(grammarAccess.getWorkSequenceAccess().getPredecessorAssignment_3(), "rule__WorkSequence__PredecessorAssignment_3");
			builder.put(grammarAccess.getWorkSequenceAccess().getSuccessorAssignment_5(), "rule__WorkSequence__SuccessorAssignment_5");
			builder.put(grammarAccess.getGuidanceAccess().getDescriptionAssignment_3(), "rule__Guidance__DescriptionAssignment_3");
			builder.put(grammarAccess.getResourcesAccess().getNameAssignment_1(), "rule__Resources__NameAssignment_1");
			builder.put(grammarAccess.getResourcesAccess().getQuantityAssignment_2(), "rule__Resources__QuantityAssignment_2");
			builder.put(grammarAccess.getUsedResourcesAccess().getQuantityAssignment_1(), "rule__UsedResources__QuantityAssignment_1");
			builder.put(grammarAccess.getUsedResourcesAccess().getRsrcesAssignment_3(), "rule__UsedResources__RsrcesAssignment_3");
		}
	}
	
	@Inject
	private NameMappings nameMappings;

	@Inject
	private TextSimplePDLGrammarAccess grammarAccess;

	@Override
	protected InternalTextSimplePDLParser createParser() {
		InternalTextSimplePDLParser result = new InternalTextSimplePDLParser(null);
		result.setGrammarAccess(grammarAccess);
		return result;
	}

	@Override
	protected String getRuleName(AbstractElement element) {
		return nameMappings.getRuleName(element);
	}

	@Override
	protected String[] getInitialHiddenTokens() {
		return new String[] { "RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT" };
	}

	public TextSimplePDLGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}

	public void setGrammarAccess(TextSimplePDLGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
	
	public NameMappings getNameMappings() {
		return nameMappings;
	}
	
	public void setNameMappings(NameMappings nameMappings) {
		this.nameMappings = nameMappings;
	}
}
