package fr.n7.simplepdl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.n7.simplepdl.services.TextSimplePDLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalTextSimplePDLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'process'", "'{'", "'}'", "'WorkDefinition'", "'WorkSequence'", "'from'", "'to'", "';'", "'Guidance'", "'description'", "'Resources'", "'uses'", "'of'", "'-'", "'s2s'", "'s2f'", "'f2s'", "'f2f'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalTextSimplePDLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalTextSimplePDLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalTextSimplePDLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalTextSimplePDL.g"; }



     	private TextSimplePDLGrammarAccess grammarAccess;

        public InternalTextSimplePDLParser(TokenStream input, TextSimplePDLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Process";
       	}

       	@Override
       	protected TextSimplePDLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleProcess"
    // InternalTextSimplePDL.g:65:1: entryRuleProcess returns [EObject current=null] : iv_ruleProcess= ruleProcess EOF ;
    public final EObject entryRuleProcess() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProcess = null;


        try {
            // InternalTextSimplePDL.g:65:48: (iv_ruleProcess= ruleProcess EOF )
            // InternalTextSimplePDL.g:66:2: iv_ruleProcess= ruleProcess EOF
            {
             newCompositeNode(grammarAccess.getProcessRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleProcess=ruleProcess();

            state._fsp--;

             current =iv_ruleProcess; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProcess"


    // $ANTLR start "ruleProcess"
    // InternalTextSimplePDL.g:72:1: ruleProcess returns [EObject current=null] : ( () otherlv_1= 'process' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_processElements_4_0= ruleProcessElement ) )* otherlv_5= '}' ) ;
    public final EObject ruleProcess() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_processElements_4_0 = null;



        	enterRule();

        try {
            // InternalTextSimplePDL.g:78:2: ( ( () otherlv_1= 'process' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_processElements_4_0= ruleProcessElement ) )* otherlv_5= '}' ) )
            // InternalTextSimplePDL.g:79:2: ( () otherlv_1= 'process' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_processElements_4_0= ruleProcessElement ) )* otherlv_5= '}' )
            {
            // InternalTextSimplePDL.g:79:2: ( () otherlv_1= 'process' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_processElements_4_0= ruleProcessElement ) )* otherlv_5= '}' )
            // InternalTextSimplePDL.g:80:3: () otherlv_1= 'process' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_processElements_4_0= ruleProcessElement ) )* otherlv_5= '}'
            {
            // InternalTextSimplePDL.g:80:3: ()
            // InternalTextSimplePDL.g:81:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getProcessAccess().getProcessAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getProcessAccess().getProcessKeyword_1());
            		
            // InternalTextSimplePDL.g:91:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalTextSimplePDL.g:92:4: (lv_name_2_0= RULE_ID )
            {
            // InternalTextSimplePDL.g:92:4: (lv_name_2_0= RULE_ID )
            // InternalTextSimplePDL.g:93:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_2_0, grammarAccess.getProcessAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getProcessRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_3, grammarAccess.getProcessAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalTextSimplePDL.g:113:3: ( (lv_processElements_4_0= ruleProcessElement ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=14 && LA1_0<=15)||LA1_0==19||LA1_0==21) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalTextSimplePDL.g:114:4: (lv_processElements_4_0= ruleProcessElement )
            	    {
            	    // InternalTextSimplePDL.g:114:4: (lv_processElements_4_0= ruleProcessElement )
            	    // InternalTextSimplePDL.g:115:5: lv_processElements_4_0= ruleProcessElement
            	    {

            	    					newCompositeNode(grammarAccess.getProcessAccess().getProcessElementsProcessElementParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_5);
            	    lv_processElements_4_0=ruleProcessElement();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getProcessRule());
            	    					}
            	    					add(
            	    						current,
            	    						"processElements",
            	    						lv_processElements_4_0,
            	    						"fr.n7.simplepdl.TextSimplePDL.ProcessElement");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_5=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getProcessAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProcess"


    // $ANTLR start "entryRuleWorkDefinition"
    // InternalTextSimplePDL.g:140:1: entryRuleWorkDefinition returns [EObject current=null] : iv_ruleWorkDefinition= ruleWorkDefinition EOF ;
    public final EObject entryRuleWorkDefinition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWorkDefinition = null;


        try {
            // InternalTextSimplePDL.g:140:55: (iv_ruleWorkDefinition= ruleWorkDefinition EOF )
            // InternalTextSimplePDL.g:141:2: iv_ruleWorkDefinition= ruleWorkDefinition EOF
            {
             newCompositeNode(grammarAccess.getWorkDefinitionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleWorkDefinition=ruleWorkDefinition();

            state._fsp--;

             current =iv_ruleWorkDefinition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWorkDefinition"


    // $ANTLR start "ruleWorkDefinition"
    // InternalTextSimplePDL.g:147:1: ruleWorkDefinition returns [EObject current=null] : ( () otherlv_1= 'WorkDefinition' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '{' ( (lv_usedResources_4_0= ruleUsedResources ) ) otherlv_5= '}' )? ) ;
    public final EObject ruleWorkDefinition() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_usedResources_4_0 = null;



        	enterRule();

        try {
            // InternalTextSimplePDL.g:153:2: ( ( () otherlv_1= 'WorkDefinition' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '{' ( (lv_usedResources_4_0= ruleUsedResources ) ) otherlv_5= '}' )? ) )
            // InternalTextSimplePDL.g:154:2: ( () otherlv_1= 'WorkDefinition' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '{' ( (lv_usedResources_4_0= ruleUsedResources ) ) otherlv_5= '}' )? )
            {
            // InternalTextSimplePDL.g:154:2: ( () otherlv_1= 'WorkDefinition' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '{' ( (lv_usedResources_4_0= ruleUsedResources ) ) otherlv_5= '}' )? )
            // InternalTextSimplePDL.g:155:3: () otherlv_1= 'WorkDefinition' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '{' ( (lv_usedResources_4_0= ruleUsedResources ) ) otherlv_5= '}' )?
            {
            // InternalTextSimplePDL.g:155:3: ()
            // InternalTextSimplePDL.g:156:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getWorkDefinitionAccess().getWorkDefinitionAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,14,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getWorkDefinitionAccess().getWorkDefinitionKeyword_1());
            		
            // InternalTextSimplePDL.g:166:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalTextSimplePDL.g:167:4: (lv_name_2_0= RULE_ID )
            {
            // InternalTextSimplePDL.g:167:4: (lv_name_2_0= RULE_ID )
            // InternalTextSimplePDL.g:168:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(lv_name_2_0, grammarAccess.getWorkDefinitionAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getWorkDefinitionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalTextSimplePDL.g:184:3: (otherlv_3= '{' ( (lv_usedResources_4_0= ruleUsedResources ) ) otherlv_5= '}' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==12) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalTextSimplePDL.g:185:4: otherlv_3= '{' ( (lv_usedResources_4_0= ruleUsedResources ) ) otherlv_5= '}'
                    {
                    otherlv_3=(Token)match(input,12,FOLLOW_7); 

                    				newLeafNode(otherlv_3, grammarAccess.getWorkDefinitionAccess().getLeftCurlyBracketKeyword_3_0());
                    			
                    // InternalTextSimplePDL.g:189:4: ( (lv_usedResources_4_0= ruleUsedResources ) )
                    // InternalTextSimplePDL.g:190:5: (lv_usedResources_4_0= ruleUsedResources )
                    {
                    // InternalTextSimplePDL.g:190:5: (lv_usedResources_4_0= ruleUsedResources )
                    // InternalTextSimplePDL.g:191:6: lv_usedResources_4_0= ruleUsedResources
                    {

                    						newCompositeNode(grammarAccess.getWorkDefinitionAccess().getUsedResourcesUsedResourcesParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_usedResources_4_0=ruleUsedResources();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getWorkDefinitionRule());
                    						}
                    						add(
                    							current,
                    							"usedResources",
                    							lv_usedResources_4_0,
                    							"fr.n7.simplepdl.TextSimplePDL.UsedResources");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_5=(Token)match(input,13,FOLLOW_2); 

                    				newLeafNode(otherlv_5, grammarAccess.getWorkDefinitionAccess().getRightCurlyBracketKeyword_3_2());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWorkDefinition"


    // $ANTLR start "entryRuleWorkSequence"
    // InternalTextSimplePDL.g:217:1: entryRuleWorkSequence returns [EObject current=null] : iv_ruleWorkSequence= ruleWorkSequence EOF ;
    public final EObject entryRuleWorkSequence() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWorkSequence = null;


        try {
            // InternalTextSimplePDL.g:217:53: (iv_ruleWorkSequence= ruleWorkSequence EOF )
            // InternalTextSimplePDL.g:218:2: iv_ruleWorkSequence= ruleWorkSequence EOF
            {
             newCompositeNode(grammarAccess.getWorkSequenceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleWorkSequence=ruleWorkSequence();

            state._fsp--;

             current =iv_ruleWorkSequence; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWorkSequence"


    // $ANTLR start "ruleWorkSequence"
    // InternalTextSimplePDL.g:224:1: ruleWorkSequence returns [EObject current=null] : (otherlv_0= 'WorkSequence' ( (lv_linkType_1_0= ruleWorkSequenceType ) ) otherlv_2= 'from' ( (otherlv_3= RULE_ID ) ) otherlv_4= 'to' ( (otherlv_5= RULE_ID ) ) otherlv_6= ';' ) ;
    public final EObject ruleWorkSequence() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Enumerator lv_linkType_1_0 = null;



        	enterRule();

        try {
            // InternalTextSimplePDL.g:230:2: ( (otherlv_0= 'WorkSequence' ( (lv_linkType_1_0= ruleWorkSequenceType ) ) otherlv_2= 'from' ( (otherlv_3= RULE_ID ) ) otherlv_4= 'to' ( (otherlv_5= RULE_ID ) ) otherlv_6= ';' ) )
            // InternalTextSimplePDL.g:231:2: (otherlv_0= 'WorkSequence' ( (lv_linkType_1_0= ruleWorkSequenceType ) ) otherlv_2= 'from' ( (otherlv_3= RULE_ID ) ) otherlv_4= 'to' ( (otherlv_5= RULE_ID ) ) otherlv_6= ';' )
            {
            // InternalTextSimplePDL.g:231:2: (otherlv_0= 'WorkSequence' ( (lv_linkType_1_0= ruleWorkSequenceType ) ) otherlv_2= 'from' ( (otherlv_3= RULE_ID ) ) otherlv_4= 'to' ( (otherlv_5= RULE_ID ) ) otherlv_6= ';' )
            // InternalTextSimplePDL.g:232:3: otherlv_0= 'WorkSequence' ( (lv_linkType_1_0= ruleWorkSequenceType ) ) otherlv_2= 'from' ( (otherlv_3= RULE_ID ) ) otherlv_4= 'to' ( (otherlv_5= RULE_ID ) ) otherlv_6= ';'
            {
            otherlv_0=(Token)match(input,15,FOLLOW_9); 

            			newLeafNode(otherlv_0, grammarAccess.getWorkSequenceAccess().getWorkSequenceKeyword_0());
            		
            // InternalTextSimplePDL.g:236:3: ( (lv_linkType_1_0= ruleWorkSequenceType ) )
            // InternalTextSimplePDL.g:237:4: (lv_linkType_1_0= ruleWorkSequenceType )
            {
            // InternalTextSimplePDL.g:237:4: (lv_linkType_1_0= ruleWorkSequenceType )
            // InternalTextSimplePDL.g:238:5: lv_linkType_1_0= ruleWorkSequenceType
            {

            					newCompositeNode(grammarAccess.getWorkSequenceAccess().getLinkTypeWorkSequenceTypeEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_10);
            lv_linkType_1_0=ruleWorkSequenceType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getWorkSequenceRule());
            					}
            					set(
            						current,
            						"linkType",
            						lv_linkType_1_0,
            						"fr.n7.simplepdl.TextSimplePDL.WorkSequenceType");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getWorkSequenceAccess().getFromKeyword_2());
            		
            // InternalTextSimplePDL.g:259:3: ( (otherlv_3= RULE_ID ) )
            // InternalTextSimplePDL.g:260:4: (otherlv_3= RULE_ID )
            {
            // InternalTextSimplePDL.g:260:4: (otherlv_3= RULE_ID )
            // InternalTextSimplePDL.g:261:5: otherlv_3= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getWorkSequenceRule());
            					}
            				
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_11); 

            					newLeafNode(otherlv_3, grammarAccess.getWorkSequenceAccess().getPredecessorWorkDefinitionCrossReference_3_0());
            				

            }


            }

            otherlv_4=(Token)match(input,17,FOLLOW_3); 

            			newLeafNode(otherlv_4, grammarAccess.getWorkSequenceAccess().getToKeyword_4());
            		
            // InternalTextSimplePDL.g:276:3: ( (otherlv_5= RULE_ID ) )
            // InternalTextSimplePDL.g:277:4: (otherlv_5= RULE_ID )
            {
            // InternalTextSimplePDL.g:277:4: (otherlv_5= RULE_ID )
            // InternalTextSimplePDL.g:278:5: otherlv_5= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getWorkSequenceRule());
            					}
            				
            otherlv_5=(Token)match(input,RULE_ID,FOLLOW_12); 

            					newLeafNode(otherlv_5, grammarAccess.getWorkSequenceAccess().getSuccessorWorkDefinitionCrossReference_5_0());
            				

            }


            }

            otherlv_6=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getWorkSequenceAccess().getSemicolonKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWorkSequence"


    // $ANTLR start "entryRuleProcessElement"
    // InternalTextSimplePDL.g:297:1: entryRuleProcessElement returns [EObject current=null] : iv_ruleProcessElement= ruleProcessElement EOF ;
    public final EObject entryRuleProcessElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProcessElement = null;


        try {
            // InternalTextSimplePDL.g:297:55: (iv_ruleProcessElement= ruleProcessElement EOF )
            // InternalTextSimplePDL.g:298:2: iv_ruleProcessElement= ruleProcessElement EOF
            {
             newCompositeNode(grammarAccess.getProcessElementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleProcessElement=ruleProcessElement();

            state._fsp--;

             current =iv_ruleProcessElement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProcessElement"


    // $ANTLR start "ruleProcessElement"
    // InternalTextSimplePDL.g:304:1: ruleProcessElement returns [EObject current=null] : (this_WorkDefinition_0= ruleWorkDefinition | this_WorkSequence_1= ruleWorkSequence | this_Guidance_2= ruleGuidance | this_Resources_3= ruleResources ) ;
    public final EObject ruleProcessElement() throws RecognitionException {
        EObject current = null;

        EObject this_WorkDefinition_0 = null;

        EObject this_WorkSequence_1 = null;

        EObject this_Guidance_2 = null;

        EObject this_Resources_3 = null;



        	enterRule();

        try {
            // InternalTextSimplePDL.g:310:2: ( (this_WorkDefinition_0= ruleWorkDefinition | this_WorkSequence_1= ruleWorkSequence | this_Guidance_2= ruleGuidance | this_Resources_3= ruleResources ) )
            // InternalTextSimplePDL.g:311:2: (this_WorkDefinition_0= ruleWorkDefinition | this_WorkSequence_1= ruleWorkSequence | this_Guidance_2= ruleGuidance | this_Resources_3= ruleResources )
            {
            // InternalTextSimplePDL.g:311:2: (this_WorkDefinition_0= ruleWorkDefinition | this_WorkSequence_1= ruleWorkSequence | this_Guidance_2= ruleGuidance | this_Resources_3= ruleResources )
            int alt3=4;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt3=1;
                }
                break;
            case 15:
                {
                alt3=2;
                }
                break;
            case 19:
                {
                alt3=3;
                }
                break;
            case 21:
                {
                alt3=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalTextSimplePDL.g:312:3: this_WorkDefinition_0= ruleWorkDefinition
                    {

                    			newCompositeNode(grammarAccess.getProcessElementAccess().getWorkDefinitionParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_WorkDefinition_0=ruleWorkDefinition();

                    state._fsp--;


                    			current = this_WorkDefinition_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalTextSimplePDL.g:321:3: this_WorkSequence_1= ruleWorkSequence
                    {

                    			newCompositeNode(grammarAccess.getProcessElementAccess().getWorkSequenceParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_WorkSequence_1=ruleWorkSequence();

                    state._fsp--;


                    			current = this_WorkSequence_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalTextSimplePDL.g:330:3: this_Guidance_2= ruleGuidance
                    {

                    			newCompositeNode(grammarAccess.getProcessElementAccess().getGuidanceParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Guidance_2=ruleGuidance();

                    state._fsp--;


                    			current = this_Guidance_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalTextSimplePDL.g:339:3: this_Resources_3= ruleResources
                    {

                    			newCompositeNode(grammarAccess.getProcessElementAccess().getResourcesParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_Resources_3=ruleResources();

                    state._fsp--;


                    			current = this_Resources_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProcessElement"


    // $ANTLR start "entryRuleGuidance"
    // InternalTextSimplePDL.g:351:1: entryRuleGuidance returns [EObject current=null] : iv_ruleGuidance= ruleGuidance EOF ;
    public final EObject entryRuleGuidance() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGuidance = null;


        try {
            // InternalTextSimplePDL.g:351:49: (iv_ruleGuidance= ruleGuidance EOF )
            // InternalTextSimplePDL.g:352:2: iv_ruleGuidance= ruleGuidance EOF
            {
             newCompositeNode(grammarAccess.getGuidanceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGuidance=ruleGuidance();

            state._fsp--;

             current =iv_ruleGuidance; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGuidance"


    // $ANTLR start "ruleGuidance"
    // InternalTextSimplePDL.g:358:1: ruleGuidance returns [EObject current=null] : (otherlv_0= 'Guidance' otherlv_1= '{' otherlv_2= 'description' ( (lv_description_3_0= RULE_ID ) ) otherlv_4= '}' ) ;
    public final EObject ruleGuidance() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_description_3_0=null;
        Token otherlv_4=null;


        	enterRule();

        try {
            // InternalTextSimplePDL.g:364:2: ( (otherlv_0= 'Guidance' otherlv_1= '{' otherlv_2= 'description' ( (lv_description_3_0= RULE_ID ) ) otherlv_4= '}' ) )
            // InternalTextSimplePDL.g:365:2: (otherlv_0= 'Guidance' otherlv_1= '{' otherlv_2= 'description' ( (lv_description_3_0= RULE_ID ) ) otherlv_4= '}' )
            {
            // InternalTextSimplePDL.g:365:2: (otherlv_0= 'Guidance' otherlv_1= '{' otherlv_2= 'description' ( (lv_description_3_0= RULE_ID ) ) otherlv_4= '}' )
            // InternalTextSimplePDL.g:366:3: otherlv_0= 'Guidance' otherlv_1= '{' otherlv_2= 'description' ( (lv_description_3_0= RULE_ID ) ) otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,19,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getGuidanceAccess().getGuidanceKeyword_0());
            		
            otherlv_1=(Token)match(input,12,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getGuidanceAccess().getLeftCurlyBracketKeyword_1());
            		
            otherlv_2=(Token)match(input,20,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getGuidanceAccess().getDescriptionKeyword_2());
            		
            // InternalTextSimplePDL.g:378:3: ( (lv_description_3_0= RULE_ID ) )
            // InternalTextSimplePDL.g:379:4: (lv_description_3_0= RULE_ID )
            {
            // InternalTextSimplePDL.g:379:4: (lv_description_3_0= RULE_ID )
            // InternalTextSimplePDL.g:380:5: lv_description_3_0= RULE_ID
            {
            lv_description_3_0=(Token)match(input,RULE_ID,FOLLOW_8); 

            					newLeafNode(lv_description_3_0, grammarAccess.getGuidanceAccess().getDescriptionIDTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getGuidanceRule());
            					}
            					setWithLastConsumed(
            						current,
            						"description",
            						lv_description_3_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_4=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getGuidanceAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGuidance"


    // $ANTLR start "entryRuleResources"
    // InternalTextSimplePDL.g:404:1: entryRuleResources returns [EObject current=null] : iv_ruleResources= ruleResources EOF ;
    public final EObject entryRuleResources() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleResources = null;


        try {
            // InternalTextSimplePDL.g:404:50: (iv_ruleResources= ruleResources EOF )
            // InternalTextSimplePDL.g:405:2: iv_ruleResources= ruleResources EOF
            {
             newCompositeNode(grammarAccess.getResourcesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleResources=ruleResources();

            state._fsp--;

             current =iv_ruleResources; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleResources"


    // $ANTLR start "ruleResources"
    // InternalTextSimplePDL.g:411:1: ruleResources returns [EObject current=null] : (otherlv_0= 'Resources' ( (lv_name_1_0= RULE_ID ) ) ( (lv_quantity_2_0= ruleEInt ) ) ) ;
    public final EObject ruleResources() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        AntlrDatatypeRuleToken lv_quantity_2_0 = null;



        	enterRule();

        try {
            // InternalTextSimplePDL.g:417:2: ( (otherlv_0= 'Resources' ( (lv_name_1_0= RULE_ID ) ) ( (lv_quantity_2_0= ruleEInt ) ) ) )
            // InternalTextSimplePDL.g:418:2: (otherlv_0= 'Resources' ( (lv_name_1_0= RULE_ID ) ) ( (lv_quantity_2_0= ruleEInt ) ) )
            {
            // InternalTextSimplePDL.g:418:2: (otherlv_0= 'Resources' ( (lv_name_1_0= RULE_ID ) ) ( (lv_quantity_2_0= ruleEInt ) ) )
            // InternalTextSimplePDL.g:419:3: otherlv_0= 'Resources' ( (lv_name_1_0= RULE_ID ) ) ( (lv_quantity_2_0= ruleEInt ) )
            {
            otherlv_0=(Token)match(input,21,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getResourcesAccess().getResourcesKeyword_0());
            		
            // InternalTextSimplePDL.g:423:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalTextSimplePDL.g:424:4: (lv_name_1_0= RULE_ID )
            {
            // InternalTextSimplePDL.g:424:4: (lv_name_1_0= RULE_ID )
            // InternalTextSimplePDL.g:425:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_14); 

            					newLeafNode(lv_name_1_0, grammarAccess.getResourcesAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getResourcesRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalTextSimplePDL.g:441:3: ( (lv_quantity_2_0= ruleEInt ) )
            // InternalTextSimplePDL.g:442:4: (lv_quantity_2_0= ruleEInt )
            {
            // InternalTextSimplePDL.g:442:4: (lv_quantity_2_0= ruleEInt )
            // InternalTextSimplePDL.g:443:5: lv_quantity_2_0= ruleEInt
            {

            					newCompositeNode(grammarAccess.getResourcesAccess().getQuantityEIntParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_quantity_2_0=ruleEInt();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getResourcesRule());
            					}
            					set(
            						current,
            						"quantity",
            						lv_quantity_2_0,
            						"fr.n7.simplepdl.TextSimplePDL.EInt");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleResources"


    // $ANTLR start "entryRuleUsedResources"
    // InternalTextSimplePDL.g:464:1: entryRuleUsedResources returns [EObject current=null] : iv_ruleUsedResources= ruleUsedResources EOF ;
    public final EObject entryRuleUsedResources() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUsedResources = null;


        try {
            // InternalTextSimplePDL.g:464:54: (iv_ruleUsedResources= ruleUsedResources EOF )
            // InternalTextSimplePDL.g:465:2: iv_ruleUsedResources= ruleUsedResources EOF
            {
             newCompositeNode(grammarAccess.getUsedResourcesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUsedResources=ruleUsedResources();

            state._fsp--;

             current =iv_ruleUsedResources; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUsedResources"


    // $ANTLR start "ruleUsedResources"
    // InternalTextSimplePDL.g:471:1: ruleUsedResources returns [EObject current=null] : (otherlv_0= 'uses' ( (lv_quantity_1_0= ruleEInt ) ) otherlv_2= 'of' ( (otherlv_3= RULE_ID ) ) otherlv_4= ';' ) ;
    public final EObject ruleUsedResources() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        AntlrDatatypeRuleToken lv_quantity_1_0 = null;



        	enterRule();

        try {
            // InternalTextSimplePDL.g:477:2: ( (otherlv_0= 'uses' ( (lv_quantity_1_0= ruleEInt ) ) otherlv_2= 'of' ( (otherlv_3= RULE_ID ) ) otherlv_4= ';' ) )
            // InternalTextSimplePDL.g:478:2: (otherlv_0= 'uses' ( (lv_quantity_1_0= ruleEInt ) ) otherlv_2= 'of' ( (otherlv_3= RULE_ID ) ) otherlv_4= ';' )
            {
            // InternalTextSimplePDL.g:478:2: (otherlv_0= 'uses' ( (lv_quantity_1_0= ruleEInt ) ) otherlv_2= 'of' ( (otherlv_3= RULE_ID ) ) otherlv_4= ';' )
            // InternalTextSimplePDL.g:479:3: otherlv_0= 'uses' ( (lv_quantity_1_0= ruleEInt ) ) otherlv_2= 'of' ( (otherlv_3= RULE_ID ) ) otherlv_4= ';'
            {
            otherlv_0=(Token)match(input,22,FOLLOW_14); 

            			newLeafNode(otherlv_0, grammarAccess.getUsedResourcesAccess().getUsesKeyword_0());
            		
            // InternalTextSimplePDL.g:483:3: ( (lv_quantity_1_0= ruleEInt ) )
            // InternalTextSimplePDL.g:484:4: (lv_quantity_1_0= ruleEInt )
            {
            // InternalTextSimplePDL.g:484:4: (lv_quantity_1_0= ruleEInt )
            // InternalTextSimplePDL.g:485:5: lv_quantity_1_0= ruleEInt
            {

            					newCompositeNode(grammarAccess.getUsedResourcesAccess().getQuantityEIntParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_15);
            lv_quantity_1_0=ruleEInt();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getUsedResourcesRule());
            					}
            					set(
            						current,
            						"quantity",
            						lv_quantity_1_0,
            						"fr.n7.simplepdl.TextSimplePDL.EInt");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,23,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getUsedResourcesAccess().getOfKeyword_2());
            		
            // InternalTextSimplePDL.g:506:3: ( (otherlv_3= RULE_ID ) )
            // InternalTextSimplePDL.g:507:4: (otherlv_3= RULE_ID )
            {
            // InternalTextSimplePDL.g:507:4: (otherlv_3= RULE_ID )
            // InternalTextSimplePDL.g:508:5: otherlv_3= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getUsedResourcesRule());
            					}
            				
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_12); 

            					newLeafNode(otherlv_3, grammarAccess.getUsedResourcesAccess().getRsrcesResourcesCrossReference_3_0());
            				

            }


            }

            otherlv_4=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getUsedResourcesAccess().getSemicolonKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUsedResources"


    // $ANTLR start "entryRuleEInt"
    // InternalTextSimplePDL.g:527:1: entryRuleEInt returns [String current=null] : iv_ruleEInt= ruleEInt EOF ;
    public final String entryRuleEInt() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEInt = null;


        try {
            // InternalTextSimplePDL.g:527:44: (iv_ruleEInt= ruleEInt EOF )
            // InternalTextSimplePDL.g:528:2: iv_ruleEInt= ruleEInt EOF
            {
             newCompositeNode(grammarAccess.getEIntRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEInt=ruleEInt();

            state._fsp--;

             current =iv_ruleEInt.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEInt"


    // $ANTLR start "ruleEInt"
    // InternalTextSimplePDL.g:534:1: ruleEInt returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? this_INT_1= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleEInt() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;


        	enterRule();

        try {
            // InternalTextSimplePDL.g:540:2: ( ( (kw= '-' )? this_INT_1= RULE_INT ) )
            // InternalTextSimplePDL.g:541:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            {
            // InternalTextSimplePDL.g:541:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            // InternalTextSimplePDL.g:542:3: (kw= '-' )? this_INT_1= RULE_INT
            {
            // InternalTextSimplePDL.g:542:3: (kw= '-' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==24) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalTextSimplePDL.g:543:4: kw= '-'
                    {
                    kw=(Token)match(input,24,FOLLOW_16); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getEIntAccess().getHyphenMinusKeyword_0());
                    			

                    }
                    break;

            }

            this_INT_1=(Token)match(input,RULE_INT,FOLLOW_2); 

            			current.merge(this_INT_1);
            		

            			newLeafNode(this_INT_1, grammarAccess.getEIntAccess().getINTTerminalRuleCall_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEInt"


    // $ANTLR start "ruleWorkSequenceType"
    // InternalTextSimplePDL.g:560:1: ruleWorkSequenceType returns [Enumerator current=null] : ( (enumLiteral_0= 's2s' ) | (enumLiteral_1= 's2f' ) | (enumLiteral_2= 'f2s' ) | (enumLiteral_3= 'f2f' ) ) ;
    public final Enumerator ruleWorkSequenceType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;


        	enterRule();

        try {
            // InternalTextSimplePDL.g:566:2: ( ( (enumLiteral_0= 's2s' ) | (enumLiteral_1= 's2f' ) | (enumLiteral_2= 'f2s' ) | (enumLiteral_3= 'f2f' ) ) )
            // InternalTextSimplePDL.g:567:2: ( (enumLiteral_0= 's2s' ) | (enumLiteral_1= 's2f' ) | (enumLiteral_2= 'f2s' ) | (enumLiteral_3= 'f2f' ) )
            {
            // InternalTextSimplePDL.g:567:2: ( (enumLiteral_0= 's2s' ) | (enumLiteral_1= 's2f' ) | (enumLiteral_2= 'f2s' ) | (enumLiteral_3= 'f2f' ) )
            int alt5=4;
            switch ( input.LA(1) ) {
            case 25:
                {
                alt5=1;
                }
                break;
            case 26:
                {
                alt5=2;
                }
                break;
            case 27:
                {
                alt5=3;
                }
                break;
            case 28:
                {
                alt5=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalTextSimplePDL.g:568:3: (enumLiteral_0= 's2s' )
                    {
                    // InternalTextSimplePDL.g:568:3: (enumLiteral_0= 's2s' )
                    // InternalTextSimplePDL.g:569:4: enumLiteral_0= 's2s'
                    {
                    enumLiteral_0=(Token)match(input,25,FOLLOW_2); 

                    				current = grammarAccess.getWorkSequenceTypeAccess().getStart2startEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getWorkSequenceTypeAccess().getStart2startEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalTextSimplePDL.g:576:3: (enumLiteral_1= 's2f' )
                    {
                    // InternalTextSimplePDL.g:576:3: (enumLiteral_1= 's2f' )
                    // InternalTextSimplePDL.g:577:4: enumLiteral_1= 's2f'
                    {
                    enumLiteral_1=(Token)match(input,26,FOLLOW_2); 

                    				current = grammarAccess.getWorkSequenceTypeAccess().getStart2finishEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getWorkSequenceTypeAccess().getStart2finishEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalTextSimplePDL.g:584:3: (enumLiteral_2= 'f2s' )
                    {
                    // InternalTextSimplePDL.g:584:3: (enumLiteral_2= 'f2s' )
                    // InternalTextSimplePDL.g:585:4: enumLiteral_2= 'f2s'
                    {
                    enumLiteral_2=(Token)match(input,27,FOLLOW_2); 

                    				current = grammarAccess.getWorkSequenceTypeAccess().getFinish2startEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getWorkSequenceTypeAccess().getFinish2startEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalTextSimplePDL.g:592:3: (enumLiteral_3= 'f2f' )
                    {
                    // InternalTextSimplePDL.g:592:3: (enumLiteral_3= 'f2f' )
                    // InternalTextSimplePDL.g:593:4: enumLiteral_3= 'f2f'
                    {
                    enumLiteral_3=(Token)match(input,28,FOLLOW_2); 

                    				current = grammarAccess.getWorkSequenceTypeAccess().getFinish2finishEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getWorkSequenceTypeAccess().getFinish2finishEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWorkSequenceType"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x000000000028E000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x000000001E000000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000001000020L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000000020L});

}