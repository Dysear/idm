/**
 * generated by Xtext 2.29.0
 */
package fr.n7.simplepdl.textSimplePDL;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.n7.simplepdl.textSimplePDL.TextSimplePDLFactory
 * @model kind="package"
 * @generated
 */
public interface TextSimplePDLPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "textSimplePDL";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.n7.fr/simplepdl/TextSimplePDL";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "textSimplePDL";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  TextSimplePDLPackage eINSTANCE = fr.n7.simplepdl.textSimplePDL.impl.TextSimplePDLPackageImpl.init();

  /**
   * The meta object id for the '{@link fr.n7.simplepdl.textSimplePDL.impl.ProcessImpl <em>Process</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.n7.simplepdl.textSimplePDL.impl.ProcessImpl
   * @see fr.n7.simplepdl.textSimplePDL.impl.TextSimplePDLPackageImpl#getProcess()
   * @generated
   */
  int PROCESS = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCESS__NAME = 0;

  /**
   * The feature id for the '<em><b>Process Elements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCESS__PROCESS_ELEMENTS = 1;

  /**
   * The number of structural features of the '<em>Process</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCESS_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link fr.n7.simplepdl.textSimplePDL.impl.ProcessElementImpl <em>Process Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.n7.simplepdl.textSimplePDL.impl.ProcessElementImpl
   * @see fr.n7.simplepdl.textSimplePDL.impl.TextSimplePDLPackageImpl#getProcessElement()
   * @generated
   */
  int PROCESS_ELEMENT = 3;

  /**
   * The number of structural features of the '<em>Process Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCESS_ELEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.n7.simplepdl.textSimplePDL.impl.WorkDefinitionImpl <em>Work Definition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.n7.simplepdl.textSimplePDL.impl.WorkDefinitionImpl
   * @see fr.n7.simplepdl.textSimplePDL.impl.TextSimplePDLPackageImpl#getWorkDefinition()
   * @generated
   */
  int WORK_DEFINITION = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WORK_DEFINITION__NAME = PROCESS_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Used Resources</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WORK_DEFINITION__USED_RESOURCES = PROCESS_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Work Definition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WORK_DEFINITION_FEATURE_COUNT = PROCESS_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.n7.simplepdl.textSimplePDL.impl.WorkSequenceImpl <em>Work Sequence</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.n7.simplepdl.textSimplePDL.impl.WorkSequenceImpl
   * @see fr.n7.simplepdl.textSimplePDL.impl.TextSimplePDLPackageImpl#getWorkSequence()
   * @generated
   */
  int WORK_SEQUENCE = 2;

  /**
   * The feature id for the '<em><b>Link Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WORK_SEQUENCE__LINK_TYPE = PROCESS_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Predecessor</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WORK_SEQUENCE__PREDECESSOR = PROCESS_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Successor</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WORK_SEQUENCE__SUCCESSOR = PROCESS_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Work Sequence</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WORK_SEQUENCE_FEATURE_COUNT = PROCESS_ELEMENT_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.n7.simplepdl.textSimplePDL.impl.GuidanceImpl <em>Guidance</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.n7.simplepdl.textSimplePDL.impl.GuidanceImpl
   * @see fr.n7.simplepdl.textSimplePDL.impl.TextSimplePDLPackageImpl#getGuidance()
   * @generated
   */
  int GUIDANCE = 4;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUIDANCE__DESCRIPTION = PROCESS_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Guidance</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUIDANCE_FEATURE_COUNT = PROCESS_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.n7.simplepdl.textSimplePDL.impl.ResourcesImpl <em>Resources</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.n7.simplepdl.textSimplePDL.impl.ResourcesImpl
   * @see fr.n7.simplepdl.textSimplePDL.impl.TextSimplePDLPackageImpl#getResources()
   * @generated
   */
  int RESOURCES = 5;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESOURCES__NAME = PROCESS_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Quantity</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESOURCES__QUANTITY = PROCESS_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Resources</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESOURCES_FEATURE_COUNT = PROCESS_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.n7.simplepdl.textSimplePDL.impl.UsedResourcesImpl <em>Used Resources</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.n7.simplepdl.textSimplePDL.impl.UsedResourcesImpl
   * @see fr.n7.simplepdl.textSimplePDL.impl.TextSimplePDLPackageImpl#getUsedResources()
   * @generated
   */
  int USED_RESOURCES = 6;

  /**
   * The feature id for the '<em><b>Quantity</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int USED_RESOURCES__QUANTITY = 0;

  /**
   * The feature id for the '<em><b>Rsrces</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int USED_RESOURCES__RSRCES = 1;

  /**
   * The number of structural features of the '<em>Used Resources</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int USED_RESOURCES_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link fr.n7.simplepdl.textSimplePDL.WorkSequenceType <em>Work Sequence Type</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.n7.simplepdl.textSimplePDL.WorkSequenceType
   * @see fr.n7.simplepdl.textSimplePDL.impl.TextSimplePDLPackageImpl#getWorkSequenceType()
   * @generated
   */
  int WORK_SEQUENCE_TYPE = 7;


  /**
   * Returns the meta object for class '{@link fr.n7.simplepdl.textSimplePDL.Process <em>Process</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Process</em>'.
   * @see fr.n7.simplepdl.textSimplePDL.Process
   * @generated
   */
  EClass getProcess();

  /**
   * Returns the meta object for the attribute '{@link fr.n7.simplepdl.textSimplePDL.Process#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.n7.simplepdl.textSimplePDL.Process#getName()
   * @see #getProcess()
   * @generated
   */
  EAttribute getProcess_Name();

  /**
   * Returns the meta object for the containment reference list '{@link fr.n7.simplepdl.textSimplePDL.Process#getProcessElements <em>Process Elements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Process Elements</em>'.
   * @see fr.n7.simplepdl.textSimplePDL.Process#getProcessElements()
   * @see #getProcess()
   * @generated
   */
  EReference getProcess_ProcessElements();

  /**
   * Returns the meta object for class '{@link fr.n7.simplepdl.textSimplePDL.WorkDefinition <em>Work Definition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Work Definition</em>'.
   * @see fr.n7.simplepdl.textSimplePDL.WorkDefinition
   * @generated
   */
  EClass getWorkDefinition();

  /**
   * Returns the meta object for the attribute '{@link fr.n7.simplepdl.textSimplePDL.WorkDefinition#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.n7.simplepdl.textSimplePDL.WorkDefinition#getName()
   * @see #getWorkDefinition()
   * @generated
   */
  EAttribute getWorkDefinition_Name();

  /**
   * Returns the meta object for the containment reference list '{@link fr.n7.simplepdl.textSimplePDL.WorkDefinition#getUsedResources <em>Used Resources</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Used Resources</em>'.
   * @see fr.n7.simplepdl.textSimplePDL.WorkDefinition#getUsedResources()
   * @see #getWorkDefinition()
   * @generated
   */
  EReference getWorkDefinition_UsedResources();

  /**
   * Returns the meta object for class '{@link fr.n7.simplepdl.textSimplePDL.WorkSequence <em>Work Sequence</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Work Sequence</em>'.
   * @see fr.n7.simplepdl.textSimplePDL.WorkSequence
   * @generated
   */
  EClass getWorkSequence();

  /**
   * Returns the meta object for the attribute '{@link fr.n7.simplepdl.textSimplePDL.WorkSequence#getLinkType <em>Link Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Link Type</em>'.
   * @see fr.n7.simplepdl.textSimplePDL.WorkSequence#getLinkType()
   * @see #getWorkSequence()
   * @generated
   */
  EAttribute getWorkSequence_LinkType();

  /**
   * Returns the meta object for the reference '{@link fr.n7.simplepdl.textSimplePDL.WorkSequence#getPredecessor <em>Predecessor</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Predecessor</em>'.
   * @see fr.n7.simplepdl.textSimplePDL.WorkSequence#getPredecessor()
   * @see #getWorkSequence()
   * @generated
   */
  EReference getWorkSequence_Predecessor();

  /**
   * Returns the meta object for the reference '{@link fr.n7.simplepdl.textSimplePDL.WorkSequence#getSuccessor <em>Successor</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Successor</em>'.
   * @see fr.n7.simplepdl.textSimplePDL.WorkSequence#getSuccessor()
   * @see #getWorkSequence()
   * @generated
   */
  EReference getWorkSequence_Successor();

  /**
   * Returns the meta object for class '{@link fr.n7.simplepdl.textSimplePDL.ProcessElement <em>Process Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Process Element</em>'.
   * @see fr.n7.simplepdl.textSimplePDL.ProcessElement
   * @generated
   */
  EClass getProcessElement();

  /**
   * Returns the meta object for class '{@link fr.n7.simplepdl.textSimplePDL.Guidance <em>Guidance</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Guidance</em>'.
   * @see fr.n7.simplepdl.textSimplePDL.Guidance
   * @generated
   */
  EClass getGuidance();

  /**
   * Returns the meta object for the attribute '{@link fr.n7.simplepdl.textSimplePDL.Guidance#getDescription <em>Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Description</em>'.
   * @see fr.n7.simplepdl.textSimplePDL.Guidance#getDescription()
   * @see #getGuidance()
   * @generated
   */
  EAttribute getGuidance_Description();

  /**
   * Returns the meta object for class '{@link fr.n7.simplepdl.textSimplePDL.Resources <em>Resources</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Resources</em>'.
   * @see fr.n7.simplepdl.textSimplePDL.Resources
   * @generated
   */
  EClass getResources();

  /**
   * Returns the meta object for the attribute '{@link fr.n7.simplepdl.textSimplePDL.Resources#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.n7.simplepdl.textSimplePDL.Resources#getName()
   * @see #getResources()
   * @generated
   */
  EAttribute getResources_Name();

  /**
   * Returns the meta object for the attribute '{@link fr.n7.simplepdl.textSimplePDL.Resources#getQuantity <em>Quantity</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Quantity</em>'.
   * @see fr.n7.simplepdl.textSimplePDL.Resources#getQuantity()
   * @see #getResources()
   * @generated
   */
  EAttribute getResources_Quantity();

  /**
   * Returns the meta object for class '{@link fr.n7.simplepdl.textSimplePDL.UsedResources <em>Used Resources</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Used Resources</em>'.
   * @see fr.n7.simplepdl.textSimplePDL.UsedResources
   * @generated
   */
  EClass getUsedResources();

  /**
   * Returns the meta object for the attribute '{@link fr.n7.simplepdl.textSimplePDL.UsedResources#getQuantity <em>Quantity</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Quantity</em>'.
   * @see fr.n7.simplepdl.textSimplePDL.UsedResources#getQuantity()
   * @see #getUsedResources()
   * @generated
   */
  EAttribute getUsedResources_Quantity();

  /**
   * Returns the meta object for the reference '{@link fr.n7.simplepdl.textSimplePDL.UsedResources#getRsrces <em>Rsrces</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Rsrces</em>'.
   * @see fr.n7.simplepdl.textSimplePDL.UsedResources#getRsrces()
   * @see #getUsedResources()
   * @generated
   */
  EReference getUsedResources_Rsrces();

  /**
   * Returns the meta object for enum '{@link fr.n7.simplepdl.textSimplePDL.WorkSequenceType <em>Work Sequence Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Work Sequence Type</em>'.
   * @see fr.n7.simplepdl.textSimplePDL.WorkSequenceType
   * @generated
   */
  EEnum getWorkSequenceType();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  TextSimplePDLFactory getTextSimplePDLFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link fr.n7.simplepdl.textSimplePDL.impl.ProcessImpl <em>Process</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.n7.simplepdl.textSimplePDL.impl.ProcessImpl
     * @see fr.n7.simplepdl.textSimplePDL.impl.TextSimplePDLPackageImpl#getProcess()
     * @generated
     */
    EClass PROCESS = eINSTANCE.getProcess();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PROCESS__NAME = eINSTANCE.getProcess_Name();

    /**
     * The meta object literal for the '<em><b>Process Elements</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROCESS__PROCESS_ELEMENTS = eINSTANCE.getProcess_ProcessElements();

    /**
     * The meta object literal for the '{@link fr.n7.simplepdl.textSimplePDL.impl.WorkDefinitionImpl <em>Work Definition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.n7.simplepdl.textSimplePDL.impl.WorkDefinitionImpl
     * @see fr.n7.simplepdl.textSimplePDL.impl.TextSimplePDLPackageImpl#getWorkDefinition()
     * @generated
     */
    EClass WORK_DEFINITION = eINSTANCE.getWorkDefinition();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute WORK_DEFINITION__NAME = eINSTANCE.getWorkDefinition_Name();

    /**
     * The meta object literal for the '<em><b>Used Resources</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WORK_DEFINITION__USED_RESOURCES = eINSTANCE.getWorkDefinition_UsedResources();

    /**
     * The meta object literal for the '{@link fr.n7.simplepdl.textSimplePDL.impl.WorkSequenceImpl <em>Work Sequence</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.n7.simplepdl.textSimplePDL.impl.WorkSequenceImpl
     * @see fr.n7.simplepdl.textSimplePDL.impl.TextSimplePDLPackageImpl#getWorkSequence()
     * @generated
     */
    EClass WORK_SEQUENCE = eINSTANCE.getWorkSequence();

    /**
     * The meta object literal for the '<em><b>Link Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute WORK_SEQUENCE__LINK_TYPE = eINSTANCE.getWorkSequence_LinkType();

    /**
     * The meta object literal for the '<em><b>Predecessor</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WORK_SEQUENCE__PREDECESSOR = eINSTANCE.getWorkSequence_Predecessor();

    /**
     * The meta object literal for the '<em><b>Successor</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WORK_SEQUENCE__SUCCESSOR = eINSTANCE.getWorkSequence_Successor();

    /**
     * The meta object literal for the '{@link fr.n7.simplepdl.textSimplePDL.impl.ProcessElementImpl <em>Process Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.n7.simplepdl.textSimplePDL.impl.ProcessElementImpl
     * @see fr.n7.simplepdl.textSimplePDL.impl.TextSimplePDLPackageImpl#getProcessElement()
     * @generated
     */
    EClass PROCESS_ELEMENT = eINSTANCE.getProcessElement();

    /**
     * The meta object literal for the '{@link fr.n7.simplepdl.textSimplePDL.impl.GuidanceImpl <em>Guidance</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.n7.simplepdl.textSimplePDL.impl.GuidanceImpl
     * @see fr.n7.simplepdl.textSimplePDL.impl.TextSimplePDLPackageImpl#getGuidance()
     * @generated
     */
    EClass GUIDANCE = eINSTANCE.getGuidance();

    /**
     * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GUIDANCE__DESCRIPTION = eINSTANCE.getGuidance_Description();

    /**
     * The meta object literal for the '{@link fr.n7.simplepdl.textSimplePDL.impl.ResourcesImpl <em>Resources</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.n7.simplepdl.textSimplePDL.impl.ResourcesImpl
     * @see fr.n7.simplepdl.textSimplePDL.impl.TextSimplePDLPackageImpl#getResources()
     * @generated
     */
    EClass RESOURCES = eINSTANCE.getResources();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RESOURCES__NAME = eINSTANCE.getResources_Name();

    /**
     * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RESOURCES__QUANTITY = eINSTANCE.getResources_Quantity();

    /**
     * The meta object literal for the '{@link fr.n7.simplepdl.textSimplePDL.impl.UsedResourcesImpl <em>Used Resources</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.n7.simplepdl.textSimplePDL.impl.UsedResourcesImpl
     * @see fr.n7.simplepdl.textSimplePDL.impl.TextSimplePDLPackageImpl#getUsedResources()
     * @generated
     */
    EClass USED_RESOURCES = eINSTANCE.getUsedResources();

    /**
     * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute USED_RESOURCES__QUANTITY = eINSTANCE.getUsedResources_Quantity();

    /**
     * The meta object literal for the '<em><b>Rsrces</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference USED_RESOURCES__RSRCES = eINSTANCE.getUsedResources_Rsrces();

    /**
     * The meta object literal for the '{@link fr.n7.simplepdl.textSimplePDL.WorkSequenceType <em>Work Sequence Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.n7.simplepdl.textSimplePDL.WorkSequenceType
     * @see fr.n7.simplepdl.textSimplePDL.impl.TextSimplePDLPackageImpl#getWorkSequenceType()
     * @generated
     */
    EEnum WORK_SEQUENCE_TYPE = eINSTANCE.getWorkSequenceType();

  }

} //TextSimplePDLPackage
