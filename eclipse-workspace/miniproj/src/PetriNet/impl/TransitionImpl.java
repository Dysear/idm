/**
 */
package PetriNet.impl;

import PetriNet.Arc;
import PetriNet.PetriNetPackage;
import PetriNet.Transition;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link PetriNet.impl.TransitionImpl#getName <em>Name</em>}</li>
 *   <li>{@link PetriNet.impl.TransitionImpl#getUpBound <em>Up Bound</em>}</li>
 *   <li>{@link PetriNet.impl.TransitionImpl#getLowBound <em>Low Bound</em>}</li>
 *   <li>{@link PetriNet.impl.TransitionImpl#getToArcs <em>To Arcs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransitionImpl extends MinimalEObjectImpl.Container implements Transition {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getUpBound() <em>Up Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpBound()
	 * @generated
	 * @ordered
	 */
	protected static final Integer UP_BOUND_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUpBound() <em>Up Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpBound()
	 * @generated
	 * @ordered
	 */
	protected Integer upBound = UP_BOUND_EDEFAULT;

	/**
	 * The default value of the '{@link #getLowBound() <em>Low Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLowBound()
	 * @generated
	 * @ordered
	 */
	protected static final int LOW_BOUND_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLowBound() <em>Low Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLowBound()
	 * @generated
	 * @ordered
	 */
	protected int lowBound = LOW_BOUND_EDEFAULT;

	/**
	 * The cached value of the '{@link #getToArcs() <em>To Arcs</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToArcs()
	 * @generated
	 * @ordered
	 */
	protected EList<Arc> toArcs;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PetriNetPackage.Literals.TRANSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PetriNetPackage.TRANSITION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getUpBound() {
		return upBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUpBound(Integer newUpBound) {
		Integer oldUpBound = upBound;
		upBound = newUpBound;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PetriNetPackage.TRANSITION__UP_BOUND, oldUpBound, upBound));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLowBound() {
		return lowBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLowBound(int newLowBound) {
		int oldLowBound = lowBound;
		lowBound = newLowBound;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PetriNetPackage.TRANSITION__LOW_BOUND, oldLowBound, lowBound));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Arc> getToArcs() {
		if (toArcs == null) {
			toArcs = new EObjectWithInverseResolvingEList<Arc>(Arc.class, this, PetriNetPackage.TRANSITION__TO_ARCS, PetriNetPackage.ARC__TRANSITION);
		}
		return toArcs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PetriNetPackage.TRANSITION__TO_ARCS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getToArcs()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PetriNetPackage.TRANSITION__TO_ARCS:
				return ((InternalEList<?>)getToArcs()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PetriNetPackage.TRANSITION__NAME:
				return getName();
			case PetriNetPackage.TRANSITION__UP_BOUND:
				return getUpBound();
			case PetriNetPackage.TRANSITION__LOW_BOUND:
				return getLowBound();
			case PetriNetPackage.TRANSITION__TO_ARCS:
				return getToArcs();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PetriNetPackage.TRANSITION__NAME:
				setName((String)newValue);
				return;
			case PetriNetPackage.TRANSITION__UP_BOUND:
				setUpBound((Integer)newValue);
				return;
			case PetriNetPackage.TRANSITION__LOW_BOUND:
				setLowBound((Integer)newValue);
				return;
			case PetriNetPackage.TRANSITION__TO_ARCS:
				getToArcs().clear();
				getToArcs().addAll((Collection<? extends Arc>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PetriNetPackage.TRANSITION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case PetriNetPackage.TRANSITION__UP_BOUND:
				setUpBound(UP_BOUND_EDEFAULT);
				return;
			case PetriNetPackage.TRANSITION__LOW_BOUND:
				setLowBound(LOW_BOUND_EDEFAULT);
				return;
			case PetriNetPackage.TRANSITION__TO_ARCS:
				getToArcs().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PetriNetPackage.TRANSITION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case PetriNetPackage.TRANSITION__UP_BOUND:
				return UP_BOUND_EDEFAULT == null ? upBound != null : !UP_BOUND_EDEFAULT.equals(upBound);
			case PetriNetPackage.TRANSITION__LOW_BOUND:
				return lowBound != LOW_BOUND_EDEFAULT;
			case PetriNetPackage.TRANSITION__TO_ARCS:
				return toArcs != null && !toArcs.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", upBound: ");
		result.append(upBound);
		result.append(", lowBound: ");
		result.append(lowBound);
		result.append(')');
		return result.toString();
	}

} //TransitionImpl
