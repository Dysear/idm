/**
 */
package PetriNet;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link PetriNet.Transition#getName <em>Name</em>}</li>
 *   <li>{@link PetriNet.Transition#getUpBound <em>Up Bound</em>}</li>
 *   <li>{@link PetriNet.Transition#getLowBound <em>Low Bound</em>}</li>
 *   <li>{@link PetriNet.Transition#getToArcs <em>To Arcs</em>}</li>
 * </ul>
 *
 * @see PetriNet.PetriNetPackage#getTransition()
 * @model
 * @generated
 */
public interface Transition extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see PetriNet.PetriNetPackage#getTransition_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link PetriNet.Transition#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Up Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Up Bound</em>' attribute.
	 * @see #setUpBound(Integer)
	 * @see PetriNet.PetriNetPackage#getTransition_UpBound()
	 * @model
	 * @generated
	 */
	Integer getUpBound();

	/**
	 * Sets the value of the '{@link PetriNet.Transition#getUpBound <em>Up Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Up Bound</em>' attribute.
	 * @see #getUpBound()
	 * @generated
	 */
	void setUpBound(Integer value);

	/**
	 * Returns the value of the '<em><b>Low Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Low Bound</em>' attribute.
	 * @see #setLowBound(int)
	 * @see PetriNet.PetriNetPackage#getTransition_LowBound()
	 * @model
	 * @generated
	 */
	int getLowBound();

	/**
	 * Sets the value of the '{@link PetriNet.Transition#getLowBound <em>Low Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Low Bound</em>' attribute.
	 * @see #getLowBound()
	 * @generated
	 */
	void setLowBound(int value);

	/**
	 * Returns the value of the '<em><b>To Arcs</b></em>' reference list.
	 * The list contents are of type {@link PetriNet.Arc}.
	 * It is bidirectional and its opposite is '{@link PetriNet.Arc#getTransition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Arcs</em>' reference list.
	 * @see PetriNet.PetriNetPackage#getTransition_ToArcs()
	 * @see PetriNet.Arc#getTransition
	 * @model opposite="transition"
	 * @generated
	 */
	EList<Arc> getToArcs();

} // Transition
