/**
 */
package PetriNet;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Place</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link PetriNet.Place#getName <em>Name</em>}</li>
 *   <li>{@link PetriNet.Place#getMark <em>Mark</em>}</li>
 *   <li>{@link PetriNet.Place#getToArcs <em>To Arcs</em>}</li>
 * </ul>
 *
 * @see PetriNet.PetriNetPackage#getPlace()
 * @model
 * @generated
 */
public interface Place extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see PetriNet.PetriNetPackage#getPlace_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link PetriNet.Place#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Mark</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mark</em>' attribute.
	 * @see #setMark(int)
	 * @see PetriNet.PetriNetPackage#getPlace_Mark()
	 * @model
	 * @generated
	 */
	int getMark();

	/**
	 * Sets the value of the '{@link PetriNet.Place#getMark <em>Mark</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mark</em>' attribute.
	 * @see #getMark()
	 * @generated
	 */
	void setMark(int value);

	/**
	 * Returns the value of the '<em><b>To Arcs</b></em>' reference list.
	 * The list contents are of type {@link PetriNet.Arc}.
	 * It is bidirectional and its opposite is '{@link PetriNet.Arc#getPlace <em>Place</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Arcs</em>' reference list.
	 * @see PetriNet.PetriNetPackage#getPlace_ToArcs()
	 * @see PetriNet.Arc#getPlace
	 * @model opposite="place"
	 * @generated
	 */
	EList<Arc> getToArcs();

} // Place
