package SPDLtoPetriNet;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import PetriNet.PetriNetPackage;
import PetriNet.Place;
import PetriNet.Transition;
import PetriNet.Arc;
import PetriNet.FlowType;
import PetriNet.PetriNetwork;
import PetriNet.PetriNetFactory;
import simplepdl.Guidance;
import simplepdl.Process;
import simplepdl.ProcessElement;
import simplepdl.Resources;
import simplepdl.WorkDefinition;
import simplepdl.WorkSequence;
import simplepdl.SimplepdlPackage;

public class SPDLtoPetriNet {

	public static class WorkDefinitionPetriPattern{
		Place pIdle = null;
		Place pRunning = null;
		Place pFinished = null;
		Place pStarted = null;

		Transition tStart = null;
		Transition tFinish = null;
	
		public Place getpIdle() {
			return pIdle;
		}
		public Place getpRunning() {
			return pRunning;
		}
		public Place getpStarted() {
			return pStarted;
		}
		public Place getpFinished() {
			return pFinished;
		}

		public Transition gettFinish() {
			return tFinish;
		}
		public Transition gettStart() {
			return tStart;
		}

		public void setpIdle(Place pIdle) {
			this.pIdle = pIdle;
		}
		public void setpRunning(Place pRunning) {
			this.pRunning = pRunning;
		}
		public void setpFinished(Place pFinished) {
			this.pFinished = pFinished;
		}
		public void setpStarted(Place pStarted) {
			this.pStarted = pStarted;
		}

		public void settStart(Transition tStart) {
			this.tStart = tStart;
		}
		public void settFinish(Transition tFinish) {
			this.tFinish = tFinish;
		}
	
	}

	public static void main(String[] args) {


		// Load SimplePDL and PetriNet in Eclipse Registry
		SimplepdlPackage simplepdlPackage = SimplepdlPackage.eINSTANCE;
		PetriNetPackage petriNetPackage = PetriNetPackage.eINSTANCE;
		
		
		// Save extension ".xmi" to be open with "XMIResourceFactoryImpl"
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("xmi", new XMIResourceFactoryImpl());
		
		// Create 2 ResourceSet for the 2 models
		ResourceSet src = new ResourceSetImpl();
		ResourceSet prc = new ResourceSetImpl();
		
		// Source model
		URI simplePDLModelURI = URI.createURI(args[0]);
		Resource simpRessource = src.getResource(simplePDLModelURI, true);
		
		// Target model
		URI petriNetmodelURI = URI.createURI("models/" + args[0] + "-java.xmi");
		Resource petriResource = prc.createResource(petriNetmodelURI);
		
		// Factory for PetriNet elements
		PetriNetFactory factory = PetriNetFactory.eINSTANCE;
		
		// Convert Process into PretiNetwork
		Process process = (Process) simpRessource.getContents().get(0);
		PetriNetwork petrinet = factory.createPetriNetwork();
		petrinet.setName(process.getName() + "-java");
		petriResource.getContents().add(petrinet);
		
		
		// Link the work definition unique name to its PetriNet conversion
		HashMap<String, WorkDefinitionPetriPattern> wdToPattern = new HashMap<String, WorkDefinitionPetriPattern>();


		// Gather work definitions and work sequences
		List<WorkDefinition> workDefinitions= new LinkedList<WorkDefinition>();
		List<WorkSequence> workSequences = new LinkedList<WorkSequence>();
		for (ProcessElement element : process.getActivities()) {
			if (element instanceof WorkDefinition) {
				WorkDefinition wd = (WorkDefinition) element;
				workDefinitions.add(wd);
			}
			else if(element instanceof WorkSequence) {
				WorkSequence ws = (WorkSequence) element;
				workSequences.add(ws);
			}
		}
		
		// Convert each work definition and its Resources into a pattern of places, transitions and arcs 
		for(WorkDefinition wd : workDefinitions) {
			createWorkDefinitionPattern(factory, petrinet, wdToPattern, wd);
		}
		
		// Convert each work sequence into a pattern of transitions
		for(WorkSequence ws : workSequences) {
			createWorkSequencePattern(factory, petrinet, wdToPattern, ws);
		}
		
		// Saving
		try {
		   	petriResource.save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	private static void createWorkSequencePattern( PetriNetFactory factory, PetriNetwork petri,  HashMap<String, WorkDefinitionPetriPattern> wdToPattern, WorkSequence ws) {

		// Get the pattern of the predecessor and the successor
		WorkDefinitionPetriPattern predecessor = wdToPattern.get(ws.getPredecessor().getName());
		WorkDefinitionPetriPattern successor = wdToPattern.get(ws.getSuccessor().getName());

		Place place = null;
		Transition transition = null;

		switch(ws.getLinkType()) {
			case START_TO_START:
				place = predecessor.pStarted;
				transition = successor.tStart;
				break;

			case START_TO_FINISH:
				place = predecessor.pStarted;
				transition = successor.tFinish;
				break;
	
			case FINISH_TO_START:
				place = predecessor.pFinished;
				transition = successor.tStart;
				break;

			case FINISH_TO_FINISH:
				place = predecessor.pFinished;
				transition = successor.tFinish;
				break;
		}

		// Create the appropriate arc
		createArc(factory, petri, true, FlowType.TO_TRANSITION, place, transition);
	}
	
	
	private static void createWorkDefinitionPattern(PetriNetFactory factory, PetriNetwork petri, HashMap<String, WorkDefinitionPetriPattern> wdToPattern, WorkDefinition wd) {

		WorkDefinitionPetriPattern pattern = new WorkDefinitionPetriPattern();

		String wdName = wd.getName();

		// Places
		Place pIdle = createPlace(factory, petri, wdName + "-idle", 1);
		Place pStarted = createPlace(factory, petri, wdName + "-started", 0);
		Place pRunning = createPlace(factory, petri, wdName + "-running", 0);
		Place pFinished = createPlace(factory, petri, wdName + "-finished", 0);
		
		// Transitions
		Transition tStart = createTransition(factory, petri, "start-" + wdName);
		Transition tFinish = createTransition(factory, petri, "finish-" + wdName);
		
		// Arcs
		createArc(factory, petri, false, FlowType.TO_TRANSITION, pIdle, tStart);
		createArc(factory, petri, false, FlowType.TO_PLACE, pRunning, tStart);
		createArc(factory, petri, false, FlowType.TO_PLACE, pStarted, tStart);
		createArc(factory, petri, false, FlowType.TO_TRANSITION, pRunning, tFinish);
		createArc(factory, petri, false, FlowType.TO_PLACE, pFinished, tFinish);
		
		//Ressources
		EList<Resources> lst = wd.getResources();
		for(Resources res : lst) {
			//Place resPlace = resMap.get(res.getRessource().getName());
			Place resPlace = createPlace(factory, petri, res.getName(), res.getQuantity());

			Arc useRessource = createArc(factory, petri, false, FlowType.TO_TRANSITION, resPlace, tStart);
			useRessource.setWeight(res.getQuantity());
			
			Arc returnRessource = createArc(factory, petri, false, FlowType.TO_PLACE, resPlace, tFinish);
			returnRessource.setWeight(res.getQuantity());
		}

		// Prepare for work sequence pattern
		pattern.setpIdle(pIdle);
		pattern.setpStarted(pStarted);
		pattern.setpRunning(pRunning);
		pattern.setpFinished(pFinished);

		pattern.settStart(tStart);
		pattern.settFinish(tFinish);

		wdToPattern.put(wdName, pattern);
	}
	
	

	private static Place createPlace(PetriNetFactory myFactory, PetriNetwork petri, String name, int mark){
		Place place = myFactory.createPlace();
		place.setName(name);
		place.setMark(mark);
		
		petri.getPlaces().add(place);
		return place;
	}
	
	
	private static Transition createTransition(PetriNetFactory myFactory, PetriNetwork petri, String name){
		Transition transition = myFactory.createTransition();
		transition.setName(name);
		
		petri.getTransitions().add(transition);
		return transition;
	}
	
	
	private static Arc createArc(PetriNetFactory myFactory, PetriNetwork petri, Boolean isReadOnly,FlowType flow, Place place, Transition transition) {
		Arc arc = myFactory.createArc();
		arc.setWeight(1);
		arc.setReadonly(isReadOnly);

		arc.setPlace(place);
		arc.setTransition(transition);
		
		petri.getArcs().add(arc);
		return arc;
	}

}


